#include<stdio.h>
#include<stdlib.h>
#define DataType char

typedef struct Node
{
    DataType data;              
    struct Node *LChild;  
    struct Node *RChild; 
}BiTNode,*BiTree;

void creatBiTree(BiTree *bt)
{
    char ch;
    ch=getchar();
    if(ch=='#') *bt=NULL;
    else{
        *bt=(BiTree)malloc(sizeof(BiTNode));
        (*bt)->data=ch;
        creatBiTree(&((*bt)->LChild));
        creatBiTree(&((*bt)->RChild));
    }
}

void PreOrder(BiTree bt)
{
    if(bt!=NULL){
        printf("%c",bt->data);
        PreOrder(bt->LChild);
        PreOrder(bt->RChild);
    }
}

void exchange(BiTree bt)
{
    BiTree p;
    if(bt==NULL) return;
    p=bt->LChild;
    bt->LChild=bt->RChild;
    bt->RChild=p;
    exchange(bt->LChild);
    exchange(bt->RChild);
}

int main()
{
    BiTree T;
    creatBiTree(&T);
    PreOrder(T);
    exchange(T);
    putchar('\n');
    PreOrder(T);
    return 0;
}