#include<stdio.h>
#include<stdlib.h>
#define DataType char
#define Stack_Size 50
int count=0;
int depth=0;

//树结构
typedef struct Node
{
    DataType data;
    struct Node *LChlid;
    struct Node *RChlid;
}BiTNode,*BiTree;

//栈结构
typedef struct
{
    BiTree elem[Stack_Size];
    int top;
}SeqStack;

void InitStack(SeqStack *S)     //初始化栈
{
    S->top=-1;
}

void Push(SeqStack *S,BiTree p)   //入栈
{
    if(S->top==Stack_Size-1) return;
    S->elem[++(S->top)]=p;
}

void Pop(SeqStack *S,BiTree *e)   //出栈
{
    if(S->top==-1) return;
    *e=S->elem[(S->top)--];
}

int IsEmpty(SeqStack S)     //判断栈空
{
    if(S.top==-1) return 1;
    else return 0;
}

void GetTop(SeqStack *S,BiTree *p)  //求栈顶元素
{
    *p=S->elem[S->top];
}

void CreateBiTree(BiTree *bt)   //建立二叉链表结构树
{
    char ch;
    ch=getchar();
    if(ch=='#')  *bt=NULL;
    else{
        *bt=(BiTree)malloc(sizeof(BiTNode));
        (*bt)->data=ch;
        CreateBiTree(&((*bt)->LChlid));
        CreateBiTree(&((*bt)->RChlid));
    }
}

void InOrder(BiTree bt)     //递归中序
{
    if(bt!=NULL){       
        InOrder(bt->LChlid);
        printf("%c",bt->data);
        InOrder(bt->RChlid);
    }
}

void PreOrder(BiTree bt)    //递归先序
{
    if(bt!=NULL){
        printf("%c",bt->data);
        PreOrder(bt->LChlid);
        PreOrder(bt->RChlid);
    }
}

void PostOrder(BiTree bt)   //递归后序
{
    if(bt!=NULL){
        PostOrder(bt->LChlid);
        PostOrder(bt->RChlid);
        printf("%c",bt->data);
    }
}

void InInOrder(BiTree root)   //非递归中序
{
    SeqStack S;
    BiTree p;
    InitStack(&S);
    p=root;
    while(p!=NULL||!IsEmpty(S)){      
        if(p!=NULL){                   //根指针进栈，遍历左子树
            Push(&S,p);
            p=p->LChlid;
        }
        else{                          //根指针退栈，访问根节点，遍历右子树
            Pop(&S,&p);
            printf("%c",p->data);
            p=p->RChlid;
        }
    }
}

void PPostOrder(BiTree root)     //非递归后序
{
    BiTree p,q;
    SeqStack S;
    q=NULL;
    p=root;
    InitStack(&S);
    while(p!=NULL||!IsEmpty(S)){
        if(p!=NULL){
            Push(&S,p);p=p->LChlid;
        }
        else{
            GetTop(&S,&p);
            if((p->RChlid==NULL)||(p->RChlid==q)){
                printf("%c",p->data);
                q=p;                    //保存到q，为下一次已处理结点前驱
                Pop(&S,&p);
                p=NULL;
            }
            else
                p=p->RChlid;
        }
    }
}

void PPreOrder(BiTree root)     //非递归先序
{
    SeqStack S; 
    BiTree p=root;
    InitStack(&S);
    while(p!=NULL||!IsEmpty(S)){
        if(p!=NULL){
            Push(&S,p);
            printf("%c",p->data);
            p=p->LChlid;
         }
        else{
            Pop(&S,&p); 
            p=p->RChlid;
        }
    }
}

int main()
{
    BiTree T;
    printf("creat:");
    CreateBiTree(&T);

    printf("digiu xianxu:");
    PreOrder(T);
    printf("\n");

    printf("digiu zhongxu:");
    InOrder(T);
    printf("\n");

    printf("digiu houxu:");
    PostOrder(T);
    printf("\n");

    printf("fei zhongxu:");
    InInOrder(T);
    printf("\n");

    printf("fei houxu:");
    PPostOrder(T);
    printf("\n");

    printf("fei xianxu:");
    PPreOrder(T);
    printf("\n");
    return 0;
}