#include<stdio.h>
#include<stdlib.h>
#define DataType char
int count=0;
int depth=0;
typedef struct Node
{
    DataType data;
    struct Node *LChlid;
    struct Node *RChlid;
}BiTNode,*BiTree;

void CreateBiTree(BiTree *bt)
{
    char ch;
    ch=getchar();
    if(ch=='#')  *bt=NULL;
    else{
        *bt=(BiTree)malloc(sizeof(BiTNode));
        (*bt)->data=ch;
        CreateBiTree(&((*bt)->LChlid));
        CreateBiTree(&((*bt)->RChlid));
    }
}

void InOrder(BiTree bt)
{
    if(bt!=NULL){       
        InOrder(bt->LChlid);
        printf("%c",bt->data);
        InOrder(bt->RChlid);
    }
}

void PreOrder(BiTree bt)
{
    if(bt!=NULL){
        printf("%c",bt->data);
        PreOrder(bt->LChlid);
        PreOrder(bt->RChlid);
    }
}

void PostOrder(BiTree bt)
{
    if(bt!=NULL){
        PostOrder(bt->LChlid);
        PostOrder(bt->RChlid);
        printf("%c",bt->data);
    }
}

int leaf(BiTree bt)
{
    if(bt!=NULL){
        if(bt->LChlid==NULL&&bt->RChlid==NULL)
            count++;
        leaf(bt->LChlid);
        leaf(bt->RChlid);
    }
    return count;
}

int PreTreeDepth(BiTree bt,int h)
{
    if(bt!=NULL){
        if(h>depth) depth=h;
        PreTreeDepth(bt->LChlid,h+1);
        PreTreeDepth(bt->RChlid,h+1);
    }
    return depth;
}

int PPreTreeDepth(BiTree bt)
{
    int hl,hr,max;
    if(bt!=NULL){
        hl=PPreTreeDepth(bt->LChlid);
        hr=PPreTreeDepth(bt->RChlid);
        max=hl>hr?hl:hr;
        return(max+1);
    }
    return 0;
}

void PrintTree(BiTree bt,int nLayer)
{
    if(bt!=NULL){
        PrintTree(bt->RChlid,nLayer+1);
        for(int i=0;i<nLayer;i++)
            printf("\t");
        printf("%c\n",bt->data);
        PrintTree(bt->LChlid,nLayer+1);
    }
}

int main()
{
    BiTree T;
    printf("creat:");
    CreateBiTree(&T);
    printf("xianxu:");
    PreOrder(T);
    printf("\n");
    printf("zhongxu:");
    InOrder(T);
    printf("\n");
    printf("houxu:");
    PostOrder(T);
    printf("\n");

    printf("yezi jiedian shumu:");
    printf("%d\n",leaf(T));

    printf("gaodu1:");
    printf("%d\n",PreTreeDepth(T,1));
    printf("gaodu2:");
    printf("%d\n",PPreTreeDepth(T));

    printf("shuzhuang da yin:\n");
    PrintTree(T,1);
}

