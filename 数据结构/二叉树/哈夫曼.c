#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#define N 20
#define M 2*N-1
typedef char *HUffmanCode[N+1];
typedef struct 
{
    int weight;     //结点的权值
    int parent;     //双亲的下标
    int LChild;     //左孩子结点的下标
    int RChild;     //右孩子结点的下标
}HTNode,HuffmanTree[M+1];

void select(HuffmanTree ht,int a,int *s1,int *s2)
{
    int p;
    *s1=ht[1].weight;
    for(int i=2;i<=a;i++){
        if((ht[i].weight<*s1)&&ht[i].parent==0)
            *s1=i;p=i;
    }
    *s2=ht[1].weight;
    for(int j=1;j<=a;j++){
        if((ht[j].weight<*s2)&&ht[j].parent==0&&ht[j].weight>ht[p].weight)
        *s2=ht[j].weight;
    }
}

void CrtHuffmanTree(HuffmanTree ht,int w[],int n)
{
    int i,m;
    int s1,s2;
    for(i=1;i<=n;i++){
        ht->weight=w[i];
        ht->parent=0;
        ht->RChild=0;
        ht->LChild=0;
    }  
    m=2*n-1;
    for(i=n+1;i<=m;i++){
        ht->LChild=0;
        ht->parent=0;
        ht->RChild=0;
        ht->weight=0;
    }

    for(i=n+1;i<=m;i++){
        select(ht,i-1,&s1,&s2);                
        ht[i].weight=ht[s1].weight+ht[s2].weight;                  
        ht[s1].parent=i;ht[s2].parent=i;
        ht[i].LChild=s1;ht[i].RChild=s2;
    }
}

void CrtHuffmanCode(HuffmanTree ht,HUffmanCode hc,int n)    
{
    int start,c,i,p;
    char *cd;
    cd=(char*)malloc(n*sizeof(char));           
    cd[n-1]='\0';                               
    
    for(i=1;i<=n;i++)                          
    {
        start=n-1;                            
        c=i;p=ht[i].parent;                    
        while(p!=0){
            --start;
            if(ht[p].LChild==c) cd[start]='0';  //左分支标0
            else    cd[start]='1';              //右分支标1
            c=p;p=ht[p].parent;                 //向上倒推
        }
        hc[i]=(char*)malloc((n-start)*sizeof(char));    //为第i个编码分配空间
        strcpy(hc[i],&cd[start]);                       //把编码复制到hc[i]中
    }
    free(cd);
}

int main()
{
    int w[]={5,7,3,2,8};
    int n=5;
    HUffmanCode hc;
    HuffmanTree T;
    CrtHuffmanTree(T,w,n);
    CrtHuffmanCode(T,hc,n);
    for(int e=0;e<n;e++){
        printf("%x\t",*hc[e]);
    }
    return 0;
}