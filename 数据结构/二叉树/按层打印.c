#include<stdio.h>
#include<stdlib.h>
#define DataType char
#define true 1
#define false 0

typedef struct Node
{
    DataType data;              
    struct Node *LChild;  
    struct Node *RChild; 
}BiTNode,*BiTree;

typedef struct QueueNode
{
    BiTree e;
    struct QueueNode *next;
}QueueNode;

typedef struct 
{
    QueueNode *front;
    QueueNode *rear;
}LinkQueue;

void InitQueue(LinkQueue *Q)
{
    Q->front=(QueueNode *)malloc(sizeof(QueueNode));
    if(Q->front!=NULL);
    {
        Q->rear=Q->front;
        Q->front->next=NULL;
    }
}

void EnterQueue(LinkQueue *Q,BiTree bt)
{
    QueueNode *NewNode;
    NewNode=(QueueNode*)malloc(sizeof(QueueNode));
    if(NewNode!=NULL)
    {
        NewNode->e=bt;
        NewNode->next=NULL;
        Q->rear->next=NewNode;
        Q->rear=NewNode;
    }
}

int DeleteQueue(LinkQueue *Q,BiTree *p)
{
    QueueNode *x;
    if(Q->front==Q->rear)
        return(false);
    x=Q->front->next;
    Q->front->next=x->next;
    if(Q->rear==x)
        Q->rear=Q->front;
    *p=x->e;
    free(x);
    return(true);
}

int IsEmpty(LinkQueue *Q)
{
    if(Q->front==Q->rear)   return true;
    else    return false;
}

void creatBiTree(BiTree *bt)
{
    char ch;
    ch=getchar();
    if(ch=='#') *bt=NULL;
    else{
        *bt=(BiTree)malloc(sizeof(BiTNode));
        (*bt)->data=ch;
        creatBiTree(&((*bt)->LChild));
        creatBiTree(&((*bt)->RChild));
    }
}

int LayerOrder(BiTree bt)
{
    LinkQueue Q;
    InitQueue(&Q);
    BiTree p;
    if(bt==NULL)    return false;
    EnterQueue(&Q,bt);
    while(!IsEmpty(&Q)){
        DeleteQueue(&Q,&p);
        printf("%c",p->data);
        if(p->LChild)   EnterQueue(&Q,p->LChild);
        if(p->RChild)   EnterQueue(&Q,p->RChild);
    }
    return true;
}

int main()
{
    BiTree T;
    creatBiTree(&T);
    LayerOrder(T);
    return 0;
}