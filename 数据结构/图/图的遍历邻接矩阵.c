#include<stdio.h>
#include<stdlib.h> 
#include<string.h>     
#define Error -1
#define OK 1          
#define True 1     
#define False 0 
int visited[20];
int i=0,j=0;

//图结构
typedef enum{DG,DN,UDG,UDN}GraphKind;  //DG表示有向图，DN表示有向网，UDG表示无向图，UDN表示无向网
typedef struct 
{
    char vertex[20];              //顶点向量
    int arcs[20][20];             //邻接矩阵
    int vexnum,arcnum;            //图的顶点数和弧度
    GraphKind Kind;               //图的种类标志
}AdjMatrix;

//队列结构
typedef struct Node
{
    char data;
    struct Node *next;
}LinkQueueNode;
typedef struct 
{
    LinkQueueNode *front;
    LinkQueueNode *rear;
}LinkQueue;

//初始化队列
void InitQueue(LinkQueue *Q)
{
    Q->front=(LinkQueueNode*)malloc(sizeof(LinkQueueNode));
    if(Q->front!=NULL){
        Q->rear=Q->front;
        Q->front->next=Q->rear;
    }
}

//入队
void EnterQueue(LinkQueue *Q,int v)
{
    LinkQueueNode *new;
    new=(LinkQueueNode*)malloc(sizeof(LinkQueueNode));
    if(new!=NULL){
        new->data=v;
        Q->rear->next=new;
        Q->rear=new;
    }
}

//出队
void DeleteQueue(LinkQueue *Q,int *v)
{
    LinkQueueNode *p;
    if(Q->front==Q->rear) return;
    p=Q->front->next;
    Q->front->next=p->next;
    if(Q->rear==p) Q->rear=Q->front;
    *v=p->data;
    free(p);
}

//判断队空
int Empty(LinkQueue Q)
{
    if(Q.front==Q.rear) return True;
    return False;
}

//求v的第一个邻接点
int FirstAdjVertex(AdjMatrix G,int v)
{
    i++,j=v;
    while(G.arcs[j][i]==0){
        i++;
    }
    return(i);
}

//求v的相对于w的下一个邻接点
int NextAdjVertex(AdjMatrix G,int v,int w)
{
    i++;
    while(G.arcs[j][i]==0){
        i++;
        if(i>G.vexnum){
            i=-1;
            break;
        }
    }
    return(i);
}

//求顶点位置
int LocateVertex(AdjMatrix *G,char v)
{
    int j=Error,k;
    for(k=0;k<G->vexnum;k++)
        if(G->vertex[k]==v)
            {j=k;break;}
    return(j);
}

//构建有向图
int CreateDN(AdjMatrix *G)      
{
    int i,j,k,weight;char v1,v2;
    printf("dingdian,hudu:(shumu) ");
    scanf("%d,%d",&(G->vexnum),&(G->arcnum));   

    for(i=0;i<G->vexnum;i++)                
        for(j=0;j<G->vexnum;j++)
            G->arcs[i][j]=0;

    for(i=0;i<G->vexnum;i++){
        printf("dingdian: ");
        scanf(" %c",&(G->vertex[i])); }               

    for(k=0;k<G->arcnum;k++){
        printf("v1,v2: ");
        scanf(" %c, %c",&v1,&v2);  
        i=LocateVertex(G,v1);
        j=LocateVertex(G,v2);
        G->arcs[i][j]=1;           
    }
    return(OK);
}

//输出邻接矩阵
void print(AdjMatrix G)
{
    int p=G.vexnum;
    for(int i=0;i<p;i++){
        for(int j=0;j<p;j++){
            printf("%d ",G.arcs[i][j]);
        }
        printf("\n");
    }
}

void DFS(AdjMatrix G,int v0)
{
    printf("%c",G.vertex[v0]);
    visited[v0]=True;
    for(int vj=0;vj<G.vexnum;vj++)
        if(!visited[vj]&&G.arcs[v0][vj]==1)
            DFS(G,vj);
}

void BFS(AdjMatrix G,int v0)
{
    LinkQueue Q;
    int w,v;
    printf("bfs: %c",G.vertex[v0]);  
    visited[v0]=True;
    InitQueue(&Q);
    EnterQueue(&Q,v0);              //v0进队
    while(!Empty(Q)){
        DeleteQueue(&Q,&v);         //队头元素出队
        w=FirstAdjVertex(G,v);      //求v的第一个邻接点
        while(w!=-1){
            if(!visited[w]){
                printf("%c",G.vertex[w]);   
                visited[w]=True;
                EnterQueue(&Q,w);
            }
            w=NextAdjVertex(G,v,w);  //求v的相对于w的下一个邻接点
        }
        i=0;
    }
}

int main()
{
    AdjMatrix G;
    CreateDN(&G);
    print(G);
    printf("dfs: ");
    DFS(G,0);
    putchar('\n');
    memset(visited,False,sizeof(visited));
    BFS(G,0);
    return 0;
}