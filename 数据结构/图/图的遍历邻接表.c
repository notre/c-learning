#include<stdio.h>
#include<stdlib.h>
#define MAX_VERTEX_NUM 20                   //最多顶点数
#define OtherInfo int
#define VertexData char
#define Error -1 
#define True 1 
#define False 0
int visited[MAX_VERTEX_NUM]={0};

typedef enum{DG,DN,UDG,UDN} GraphKind;
typedef struct ArcNode{
    int adjvex;                             //该弧指向顶点的位置
    struct ArcNode *nextarc;                //指向下一条弧的指针
    OtherInfo info;                         //与该弧相关的信息
}ArcNode;
typedef struct VertexNode{
    VertexData data;                        //顶点数据
    ArcNode *firstarc;                      //指向该顶点第一条弧的指针
}VertexNode;
typedef struct{
    VertexNode vertex[MAX_VERTEX_NUM];
    int vernum,arcnum;                      //图的顶点数和弧数
    GraphKind DG;
}AdjList;

void print(AdjList g)
{
    int i=0;
    ArcNode *p;
    printf("\nlibjiebiao:\n");
    for(i=0;i<g.vernum;i++){
        printf("%c",g.vertex[i].data);
        p=g.vertex[i].firstarc;
        while(p!=NULL){
            printf("->%c",g.vertex[p->adjvex].data);
            p=p->nextarc;
        }
        printf("\n");
    }
    
}

int LocateVertex(AdjList *g,VertexData v)         //求顶点位置函数
{
    int j=Error,k;
    for(k=0;k<g->vernum;k++)
        if(g->vertex[k].data==v)
            {j=k;break;}
    return(j);
}

void creatList(AdjList *g)   //从终端输入n个顶点的信息和e条弧的信息，建立一个有向图的邻接表
{
    int n,e,i,k,j;
    char vt,vh;
    printf("dingdian hu(shumu):");
    scanf("%d,%d",&n,&e);       //从键盘输入图的顶点个数和弧的个数
    g->vernum=n;
    g->arcnum=e;
    for(i=0;i<n;i++){
        printf("dingdian:");
        scanf(" %c",&(g->vertex[i].data));
        g->vertex[i].firstarc=NULL;
    }
    for(k=0;k<e;k++){
        printf("hu:");
        scanf(" %c, %c",&vt,&vh);
        i=LocateVertex(g,vt);
        j=LocateVertex(g,vh);
        ArcNode *p;
        p=(ArcNode*)malloc(sizeof(ArcNode));
        p->adjvex=j;
        p->nextarc=g->vertex[i].firstarc;
        g->vertex[i].firstarc=p;
    }
}

void DFS(AdjList g,int v0)
{
    ArcNode *p;
    printf("%c",g.vertex[v0].data);
    visited[v0]=True;
    p=g.vertex[v0].firstarc;
    //printf("@%d",p->adjvex);
    while(p!=NULL){
        if(!visited[p->adjvex]){
            DFS(g,p->adjvex);
        }
        //printf("@");
        p=p->nextarc;
    }
}

int main()
{
    AdjList g;
    creatList(&g);
    printf("dfs: ");
    DFS(g,0);
    print(g);
    return 0;
}