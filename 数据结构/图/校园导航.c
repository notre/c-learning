#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#define MAX_VERtEX_NUM 20 
#define INFINITY 32786                 
#define Error -1;
#define True 1     
#define False 0 
int visited[MAX_VERtEX_NUM];
int stack[MAX_VERtEX_NUM];
int end,top=0;

//图结构
typedef enum{DG,DN,UDG,UDN}GraphKind;      
typedef struct ArcNode
{
    int adj;                         
    char * info;                      
}ArcNode;
typedef struct 
{
    int xi;
    char vi;
}VertexType;

typedef struct {
    VertexType vertex[MAX_VERtEX_NUM];       
    ArcNode arcs[MAX_VERtEX_NUM][MAX_VERtEX_NUM];
    int vexnum,arcnum;                      
    GraphKind kind;                        
}AdjMatrix;

int LocateVertex(AdjMatrix *G,char v)
{
    int j=Error;
    for(int k=0;k<G->vexnum;k++)
        if(G->vertex[k].vi==v)
            {j=k;break;}
    return(j);
}

//构造无向有权网
void CreateUDN(AdjMatrix *G){
    G->vexnum=6,G->arcnum=11;
    G->vertex[0].vi='A';
    G->vertex[0].xi=0;
    G->vertex[1].vi='B';
    G->vertex[1].xi=1;
    G->vertex[2].vi='C';
    G->vertex[2].xi=2;
    G->vertex[3].vi='D';
    G->vertex[3].xi=3;
    G->vertex[4].vi='E';
    G->vertex[4].xi=4;
    G->vertex[5].vi='F';
    G->vertex[5].xi=5;
    for(int i=0;i<G->vexnum;i++){
        for(int j=0;j<G->vexnum;j++){
            G->arcs[i][j].adj=INFINITY;
            G->arcs[i][j].info=NULL;
        }
    }

    char v1,v2;int i,j,w;
    i=LocateVertex(G,'A');
	j=LocateVertex(G,'B');
	w=50;
	G->arcs[i][j].adj=w;           
    G->arcs[j][i].adj=w; 
    
	i=LocateVertex(G,'A');
	j=LocateVertex(G,'C');
	w=400;
	G->arcs[i][j].adj=w;           
    G->arcs[j][i].adj=w;
    
    i=LocateVertex(G,'A');
	j=LocateVertex(G,'F');
	w=450;
	G->arcs[i][j].adj=w;           
    G->arcs[j][i].adj=w;
    
    i=LocateVertex(G,'A');
	j=LocateVertex(G,'E');
	w=300;
	G->arcs[i][j].adj=w;           
    G->arcs[j][i].adj=w;
    
    i=LocateVertex(G,'B');
	j=LocateVertex(G,'C');
	w=250;
	G->arcs[i][j].adj=w;           
    G->arcs[j][i].adj=w;
    
    i=LocateVertex(G,'B');
	j=LocateVertex(G,'D');
	w=60;
	G->arcs[i][j].adj=w;           
    G->arcs[j][i].adj=w;
    
    i=LocateVertex(G,'C');
	j=LocateVertex(G,'F');
	w=180;
	G->arcs[i][j].adj=w;           
    G->arcs[j][i].adj=w;
    
    i=LocateVertex(G,'C');
	j=LocateVertex(G,'D');
	w=150;
	G->arcs[i][j].adj=w;           
    G->arcs[j][i].adj=w;
    
    i=LocateVertex(G,'D');
	j=LocateVertex(G,'E');
	w=55;
	G->arcs[i][j].adj=w;           
    G->arcs[j][i].adj=w;
    
    i=LocateVertex(G,'D');
	j=LocateVertex(G,'F');
	w=200;
	G->arcs[i][j].adj=w;           
    G->arcs[j][i].adj=w;
    
    i=LocateVertex(G,'E');
	j=LocateVertex(G,'F');
	w=40;
	G->arcs[i][j].adj=w;           
    G->arcs[j][i].adj=w;
}

//输出函数
void PrintGrapth(AdjMatrix G)
{
    for (int i=0;i<G.vexnum;i++){
        for (int j=0;j<G.vexnum;j++){
            printf("%d\t",G.arcs[i][j].adj);
        }
        printf("\n");
    }
}

void DFS(AdjMatrix G,int v0)
{
    printf("%c",G.vertex[v0].vi);
    visited[v0]=True;
    for(int vj=0;vj<G.vexnum;vj++)
        if(!visited[vj]&&G.arcs[v0][vj].adj!=0)
            DFS(G,vj);
}

void DFS_(AdjMatrix G,int vi)
{
    if(vi==end){
        printf("-->");
        printf("A");
        for(int i=0;i<top;i++){
            printf("%c ",G.vertex[stack[i]].vi);
        }
        printf("\n");
        return;      
    }
    for(int vj=0;vj<G.vexnum;vj++){
        visited[0]=True;
        if(!visited[vj]&&G.arcs[vi][vj].adj){
            stack[top++]=G.vertex[vj].xi;
            visited[vj]=True;
            DFS_(G,G.vertex[vj].xi);
            visited[vj]=False;
            top--;
        }
    } 
}

void ShortestPath_DIJ(AdjMatrix G,int v0) 
{
	int i,n,k,min,w;
	n=G.vexnum;
	int S[n];
	int dist[n],Path[n];
	for(k=0;k<n;k++)//辅助数组的初始化 
	{
		S[k]=False;
		dist[k]=G.arcs[v0][k].adj;
		if(dist[k]<INFINITY)
			Path[k]=v0;
		else
			Path[k]=-1;
	}
	S[v0]=True;
	dist[v0]=0;

	for(i=1;i<n;i++) 
	{
		min=INFINITY;
		for(w=0;w<n;w++)
			if(!S[w]&&dist[w]<min)
			{
				k=w;
				min=dist[w];//选择一条从v0出发的最短路径，终点为v 
			}
        if(min==INFINITY) return;
		S[k]=True;  //记录已选择过 
		for(w=0;w<n;w++)    
			if(!S[w]&&(dist[k]+G.arcs[k][w].adj<dist[w])&&G.arcs[k][w].adj!=INFINITY)
			{
				dist[w]=dist[k]+G.arcs[k][w].adj;//更新dist[w] 
				Path[w]=k;//更新w的前驱为v 
			}
	}
	
	/*for(i=0;i<n;i++)//打印D[] 
		printf("dist[%d]=%d  ",i,dist[i]);
	printf("\n");
	for(i=0;i<n;i++)//打印Path 
		printf("Path[%d]=%d  ",i,Path[i]);
	printf("\n");*/

    printf("\n距离是:%d\n",dist[end]);  
    printf("走%d条路\n",Path[end]);  
}

int main()
{
    AdjMatrix G;
    CreateUDN(&G);
    PrintGrapth(G);
    DFS(G,0);
    memset(visited,False,sizeof(visited));
    putchar('\n');
    printf("想去哪?(1)(B)教学楼,(2)(C)食堂,(3)(D)图书馆,(4)(E)宿舍楼,(5)(F)操场\n");
	scanf("%d",&end);
    printf("\n路都在这里了\n");
    DFS_(G,0);
    printf("最短的那一条路在这呢:");
    ShortestPath_DIJ(G,0);
    return 0;
}