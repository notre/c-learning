
#include "pch.h"-   
#include <stdio.h>
#include <stdlib.h>
#define MAX_SIZE 12
#define MAX_NUM 10000000
//景点结构体
struct Scenic_sports {
	char name[15];
	int code;
	char info[100];
};
//查询景点信息
void get_info(Scenic_sports position[]) {
	printf("请输入要查询的景点代号：");
	int code;
	scanf("%d", &code);
	printf("%s", position[code].info);
}
//判断当前顶点是否在数组S中，也就是说是否已经被选择过
bool has_selected(int S[], int i, int k) {
	for (int j = 0; j < k; j++)
		if (i == S[j])
			return true;
	return false;
}
//从数组D中选取最小值
int search_min(int D[], int S[], int k) {	//k为数组S中元素的个数，也就是说现在已经选了k条最短路径
	int min = -1;	//若为非连通图，则返回-1
	int m = MAX_NUM;
	for (int i = 0; i < MAX_SIZE; i++) 
		if (!has_selected(S, i, k) && (D[i] < m)) {
			min = i;
			m = D[i];
		}
	
	return min;
}
//寻找最短路径
void get_shortest_path(Scenic_sports position[]) {
	int arcs[MAX_SIZE][MAX_SIZE];	//邻接矩阵，里面存放着边的权值
//初始化邻接矩阵
	for (int i = 0; i < MAX_SIZE; i++)
		for (int j = 0; j < MAX_SIZE; j++)
			arcs[i][j] = MAX_NUM;
	arcs[0][1] = arcs[1][0] = 2;
	arcs[1][2] = arcs[2][1] = 4;
	arcs[2][3] = arcs[3][2] = 3;
	arcs[2][6] = arcs[6][2] = 3;
	arcs[2][4] = arcs[4][2] = 2;
	arcs[0][5] = arcs[5][0] = 6;
	arcs[4][5] = arcs[5][4] = 3;
	arcs[4][7] = arcs[7][4] = 2;
	arcs[5][8] = arcs[8][5] = 7;
	arcs[7][8] = arcs[8][7] = 5;
	arcs[6][9] = arcs[9][6] = 6;
	arcs[9][10] = arcs[10][9] = 1;
	arcs[10][11] = arcs[11][10] = 1;
	arcs[11][8] = arcs[8][11] = 2;

	int min = 0;	//当前寻找到的最短路径的终点
	int adr1, adr2;		//查询从景点adr1到景点adr2的最短路径
	int D[MAX_SIZE] = { 0 };	//它的每个分量D[i]表示当前所找到的从始点adr1到每个终点vi的最短路径的长度
	int S[MAX_SIZE] = { 0 };	//已求得最短路径的终点的集合
	int predecessor[MAX_SIZE];	//存放顶点的前驱，方便输出最短路径，前驱的引入是本程序最重要的地方


	printf("请输入两个景点的代码：");
	scanf("%d %d",&adr1,&adr2);
	//对输入的两个景点进行交换，否则输出景点路径时为逆序输出
	int temp;
	temp = adr1;
	adr1 = adr2;
	adr2 = temp;

	for (int i = 0; i < MAX_SIZE; i++)	//初始化所有顶点的前驱都为起始景点
		predecessor[i] = adr1;

	for (int j = 0; j < MAX_SIZE; j++)
		D[j] = arcs[adr1][j];

	for (int i = 0; i < MAX_SIZE; i++) {
		for (int j = 0; (j < MAX_SIZE); j++) {
			if (j == adr1)	//防止寻找到的路径的起始点和终点相同
				continue;
			if (D[min] + arcs[min][j] < D[j]) {
				D[j] = D[min] + arcs[min][j];
				predecessor[j] = min;	//	前驱的建立
			}
		}
		min = search_min(D, S, i);	//	寻找当前最短路径，返回最短路径的终点

		if (min == -1) {	
			printf("非连通图，最短路径查询失败\n");
			exit(0);
		}

		S[i] = min;		//编号为min的顶点作为最短路径的终点，所以把此顶点放入S中
		if (min == adr2)	//如果现在选择的最短路径的终点恰好是adr2，则停止寻找最短路径，减少时间复杂度
			break;

	}
	//输出最短路径的信息
	int pre = adr2;		//注意，前面对adr1和adr2进行了交换
	printf("从%s到%s的路径长度为%d,现在输出路径信息\n", position[adr2].name,position[adr1].name,D[adr2]);
	for (int i = 0; pre != adr1; i++) {
		printf("%s-->",position[pre].name);
 		pre = predecessor[pre];
	}
	printf("%s", position[adr1].name);

}
int main()
{
	Scenic_sports position[MAX_SIZE] = {
		{"北门",0,"北门外原先有个集，后来政府说没有它就没有了"},
		{"校医院",1,"医术不敢恭维，但报销之后药挺便宜的，小病可以去校医院看"},
		{"北区公寓",2,"三个宿舍区域之一，离教学区最近，早上可以起的比东区南区晚一点"},
		{"北操",3,"来凑个数"},
		{"餐厅",4,"吃饭的地方，有一餐，二餐和清真"},
		{"东操",5,"凑数+1"},
		{"九球广场",6,"九个石头蛋，一个没有广场舞的广场"},
		{"五子顶",7,"海大崂山校区海拔最高的地方，有机会要爬上去看一看"},
		{"保研路",8,"暂无官方介绍"},
		{"行远楼",9,"海大行远楼，一跃解千愁，是一座行政楼"},
		{"孔子像",10,"每到期末考试的时候，孔子总是会收到很多贡品"},
		{"图书馆",11,"在里面自习比较宽敞，建议搬到北区"}
	};
	printf("********欢迎来到中国海洋大学景点查询系统********\n");
	printf("学校景点如下\n");
	for (int i = 0; i < MAX_SIZE; i++) 
		printf("代码：%d，名称：%s\n", position[i].code, position[i].name);
	
	printf("查询景点详细信息请按1，查询景点间的最短路径请按2\n");
	int func;
	scanf("%d", &func);
	if (func == 1)
		get_info(position);
	else if (func == 2)
		get_shortest_path(position);

}



