#include <stdio.h>
#define MAXSIZE 40

//结构体 
typedef struct {
	char ch[MAXSIZE];
	int len;
}SString;

//输入s串
void creat1(SString  *s){ 
    int i;
   printf("请输入顺序串s的长度len:");
   scanf("%d",&s->len);
   printf("请输入s串的值:\n");
   for(i=0;i<s->len;i++){
   printf("ch=");
   scanf("%c",&(s->ch[i]));
   s->ch[i]=getchar();
   }
   printf("\n");
}

//输入t串
void creat2(SString  *t){ 
	int i;
   printf("请输入顺序串t的长度len:");
   scanf("%d",&t->len);
   printf("请输入t串的值:\n");
   for(i=0;i<t->len;i++){
   printf("ch=");
   scanf("%c",&(t->ch[i]));
   t->ch[i]=getchar();
   }
   printf("\n");
}

//输出s串
void print1(SString *s){
	int i;
	printf("此时的串是:");
	if (s->len>=0)
 	for(i=0;i<s->len;i++)
	   printf("%c",s->ch[i]);
	printf("\n");
}
 
//输出t串
void print2(SString *t){
	int i;
	printf("此时的t串是:");
	if (t->len>=0)
 	for(i=0;i<t->len;i++)
	   printf("%c",t->ch[i]);
	printf("\n");
}

//插入
StrInsert(SString *s,int pos,SString t){
	int i,j;
	printf("请输入你想要插入的位置:");
	scanf("%d",&pos);
	if(pos<0||pos>s->len)
		return (0);
	if(s->len+t.len<=MAXSIZE){
		for(i=s->len+t.len-1;i>=t.len+pos;i--){
			s->ch[i]=s->ch[i-t.len];
		}
		for(i=0;i<t.len;i++){
			s->ch[i+pos]=t.ch[i];
		}
		s->len=s->len+t.len;
	}
	else if(pos+t.len<=MAXSIZE){
		for(i=MAXSIZE-1;i>t.len+pos-1;i--){
			s->ch[i]=s->ch[i-t.len];
		}
		for(i=0;i<t.len;i++){
			s->ch[i]=s->ch[i-t.len];
		}
		s->len=MAXSIZE;
	}
	else{
		for(i=0;i<MAXSIZE-pos;i++)
		s->ch[i+pos]=t.ch[i];
		s->len=MAXSIZE;; 
	}
	return (1);
} 

//删除
StrDelete(SString *s,int pos,int len){
	int i;
	printf("请输入你想要删除的位置:");
	scanf("%d",&pos);
	printf("请输入你想要删除的长度:");
	scanf("%d",&len);
	if(pos<0||pos>(s->len-len)){
		printf("删除的位置不合理!");
		return (0);
	}
	for(i=pos+len;i<s->len;i++)
	s->ch[i-len]=s->ch[i];
	s->len=s->len-len;
	return (1);
} 

//比较
StrCompare(SString *s,SString t){
	int i,j;
	for(i=0;i<s->len&&i<t.len;i++)
		if(s->ch[i]!=t.ch[i]){
			if(s->ch[i]-t.ch[i]>0)
				printf("s串比t串长!\n");
			if(s->ch[i]-t.ch[i]<0)
				printf("s串比t串短!\n");
				return (s->ch[i]-t.ch[i]);
		}
		else{
				if(s->len-t.len==0)
					printf("s串和t串相等!\n");
				if(s->len-t.len>0)
					printf("s串比t串长!\n");
				if(s->len-t.len<0)
					printf("s串比t串短!\n");
				return(s->len-t.len);
		}
} 

//简单模式匹配
StrIndex(SString s,int pos,SString t){
	int i,j,start,k;
	printf("请输入你想要开始匹配的位置:");
	scanf("%d",&pos);
	if(t.len==0){
		printf("此串为空串，是任意串的匹配穿!");
			return (0);
	}
	start=pos;
	i=start;
	j=0;
	while(i<s.len&&j<t.len)
		if(s.ch[i]==t.ch[j]){
		i++;
		j++;
		}
		else{
		start++;
		i=start;
		j=0;
		}
		if(j>=t.len){
			printf("匹配成功!\n");
			return(start);
		}
		else{
			printf("匹配不成功!\n");
			return(-1);
		}
}

//主函数
void main(){
	int pos,len;
	SString t;
	SString s;

	creat1(&s);
	print1(&s);
	creat2(&t);
	print2(&t);

	StrInsert(&s,pos,t);
	print1(&s);
	print2(&t);

	StrDelete(&s,pos,len);
	print1(&s);
	print2(&t);

	StrCompare(&s,t);

	StrIndex(s,pos,t);
	print1(&s);
	print2(&t);
}
