#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#define OK 0
#define ERROR 1
#define MAXSTRLEN 64
typedef char SString[MAXSTRLEN+1];

// 生成值为chars，名为str的串,主要作用是生成第一位存储字符串长度的串
int StrAssign(SString str, char *chars)
{
	if(strlen(chars)>MAXSTRLEN)  return ERROR;
	else
	{
		str[0]=strlen(chars);  // 字符串第一位存储字符串长度
		for(int i=1;i<=str[0];i++)
			str[i]=*(chars+i-1);  // 给str赋值
		return OK;
	}
}

// 生成next数组
void get_next(SString T, int next[])
{
	int j=1;  // j指向模式串第j个字符，初始指向T1
	int k=0;  // k指向模式串第next[j]个字符，初始指向T0
	next[1]=0;  // 若第一个字符就失配，模式串直接向后移一位
	
	while(j<=T[0])
	{
		if (k==0||T[j]==T[k])  // 第j个字符等于第k个字符时，next[j]=k,7ABCDABD	
		{
			next[j]=k;
			k++;
			j++;
		}
		else   // 第k个字符不等于第j个字符时，k回溯
			k=next[k];
	}
}

// KMP算法：返回T在S中的位置，若不存在则返回0
int Index_KMP(SString S, SString T, int pos)
{
	pos=1;
	int next[15];
	get_next(T,next);  // 获得next数组

	int j=1;  // j指向模式串T中的元素
	while (pos<=S[0]&&j<=T[0])
	{
		if(j==0||S[pos]==T[j])  // 模式串与目标串进行比较
			{pos++,j++;}
		else j=next[j];  // 当T中第j个元素与S中第pos个元素不相等（不匹配时）
	}
	if(j>T[0])  // 模式串全部匹配完毕
		{return pos=pos-T[0];}
	else  // 匹配失败
		return ERROR;
}

int main()
{
	SString target = " ";  // 目标串初始化为空串
	SString mode = " ";  // 模式串初始化为空串
	int pos,pospos;
	
	char c[MAXSTRLEN + 1];
	printf("target:");
	gets(c);
	StrAssign(target,c);
	
	printf("mode:");
	gets(c);
	StrAssign(mode,c);

	pospos=Index_KMP(target,mode,pos);
	printf("position=%d\n",pospos);

	system("pause");
	return 0;
}
