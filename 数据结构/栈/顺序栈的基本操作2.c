#include<stdio.h>
#define STACKSIZE 100
#define StackElementType int
typedef struct
{
    StackElementType data[STACKSIZE];
    int top;
}Seqstack;

void Init(Seqstack *S)
{
    S->top=-1;
}

int Push(Seqstack *S,StackElementType e)
{
    if(S->top>=STACKSIZE-1) return 0;
    S->data[++(S->top)]=e;
    return 1;
}

int Pop(Seqstack *S,StackElementType *e)
{
    if(S->top==-1)  return 0;
    e=S->data[(S->top)--];
    return 1;
}

int Gettop(Seqstack *S,StackElementType *e)
{
    if(S->top==-1) return 0;
    else    e=S->data[S->top];
    return 1;
}

void display(Seqstack *S)
{
    int index=S->top;
    while(index>-1)
    {
        printf("%d\n",S->data[index--]);
    } 
}

void re(int flag)
{
    if(flag) printf("Yes\n");
    else    printf("NO\n");
}

int main()
{
    Seqstack S;
    Init(&S);
    while(1)
    {
        int i;
        int flag,e;
        printf("exe\n1.Push\n2.Pop\n3.Gettop\n4.display\n");
        scanf("%d",&i);
        switch(i)
        {
            case 1:
                    scanf("%d",&e);
                    flag=Push(&S,e);
                    re(flag);
                    break;
            case 2:
                    flag=Pop(&S,e);
                    re(flag);
                    if(flag)
                        printf("sum is %d\n",e);
                    break;
            case 3:
                    flag=Gettop(&S,e);
                    re(flag);
                    if(flag)
                        printf("top is %d\n",e);
                    break;
            case 4:
                    display(&S);
            default:
                    break;
        }
    }
    return 0;
}