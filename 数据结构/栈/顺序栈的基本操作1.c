#include<stdio.h>
#include<stdlib.h>
#define MAXSIZE 50
#define ElemType int
typedef struct{
    ElemType data[MAXSIZE];
    int top;
}SeqStack;
void InitStack(SeqStack *s){
    s->top=-1;
}
int StackEmpty(SeqStack *s){
    if(s->top==-1)
        return 1;
    return 0;
}
int Push(SeqStack *s,ElemType e){
    if(s->top==MAXSIZE-1)
        return 0;
    s->data[++(s->top)]=e;
    return 1;
}
int Pop(SeqStack *s,ElemType *e){
    if(s->top==-1)
        return 0;
    e=s->data[(s->top)--];
    return 1;
}
int GetTop(SeqStack *s,ElemType *e){
    if(s->top==-1)
        return 0;
    e=s->data[s->top];
    return 1;
}
void check(int flag){
    if(flag)
        printf("操作成功!\n");
    else
        printf("操作失败！\n");
}
void display(SeqStack *s){
    int index=s->top;
    while(index>-1){
        printf("%d ",s->data[index--]);
    }
    printf("\n");
}
int main(){
    SeqStack s;
    printf("顺序栈的基本操作\n");
    printf("栈的初始化\n");
    InitStack(&s);
    while(1){
        int op;
        int flag;
        printf("请选择操作\n1.进栈\n2.出栈\n3.判空\n4.获取栈顶元素\n5.打印栈中所有元素\n");
        scanf("%d",&op);
        switch(op){
        case 1:
            printf("入栈元素：");
            int e;
            scanf("%d",&e);
            flag=Push(&s,e);
            check(flag);
            break;
        case 2:
            flag=Pop(&s,e);
            check(flag);
            if(flag)
                printf("栈顶元素出栈，出栈元素为：%d\n",e);
            break;
        case 3:
            flag=StackEmpty(&s);
            check(flag);
            if(flag)
                printf("栈为空！\n");
            break;
        case 4:
            flag=GetTop(&s,e);
            check(flag);
            if(flag)
                printf("栈顶元素为：%d\n",e);
            break;
        case 5:
            display(&s);
        default:
            break;
        }
    }

    return 0;
}