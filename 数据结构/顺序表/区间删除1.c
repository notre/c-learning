#include<stdio.h>
#define MAXSIZE 100
#define Elemtype int

typedef struct  
{
    Elemtype data[MAXSIZE];
    int last;
}SeqList;

void InitList(SeqList *L)
{
    L->last=0;
}

void creatList(SeqList *L,int n)
{
    int i;
    for(i=0;i<n;i++)
    {
        printf("sum is ");
        scanf("%d",&L->data[i]);
    }
    L->last=n;
}

SeqList DelList(SeqList *L,int min,int max)
{	
    int i=0,j=0;
    SeqList *q;
    while(i<=L->last)
    {
        if(L->data[i]<min||L->data[i]>max)
            {
               q->data[j]=L->data[i];
               i++;
               j++;
            }
        else i++;
    }
    q->last=j;
    return *q;
}

void printList(SeqList *q)
{
	int i;
    for(i=0;i<q->last-1;i++)
        printf("%d\t",q->data[i]);
	printf("\n");
}

int main()
{
    SeqList L,q;
    InitList(&L);
    creatList(&L,4);
    q=DelList(&L,5,10);
    printList(&q);
    return 0;
}