#include<stdio.h>
#include<string.h>
#define true 1
#define false 0
typedef struct 
{
    int i;
    char name;
}Node;
typedef struct ANode
{
    Node data;
    int last;
}ANode;

int again(ANode *a,char *hash,int e)
{
    int t=e;
    int i=true;
    while(i){
        t+=1;
        int tt=t%29;
        if(hash[tt]!=0){
            hash[tt]=a[tt].data.name;
            i=false;
        }
    }
    return true;
}

int Hash(ANode *a,char *hash)
{
    int p,q;
    for(int i=0;i<=30;i++){
        p=a[i].data.i;
        q=p%29;
        if(hash[q]==0){
            hash[q]=a[i].data.name;
        }
        else{
            again(a,hash,q);
        }
    }
    return true;
}

int main()
{
    ANode a[30];
    char hash[100];
    memset(hash,0,sizeof(hash));
    for(int i=0;i<=30;i++){
        printf("****\n");
        scanf("%d",&a[i].data.i);
        scanf("%s",&a[i].data.name);
    }
    Hash(a,hash);
    for(int j=0;j<10;j++){
            printf("%c ",hash[j]);
    }
    return 0;
}