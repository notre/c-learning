#include<stdio.h>
#define TRUE 1
#define FALSE 0
#define MAXSIZE 20

typedef struct
{
    int Element[MAXSIZE];
    int rear;
    int front;
}SeqQueue;

void InitQueue(SeqQueue *Q)
{
    Q->front=Q->rear=0;
}

int EnterQueue(SeqQueue *Q,int i)
{
    if((Q->rear+1)%MAXSIZE==Q->front)
        return(FALSE);
    Q->Element[Q->rear]=i;
    Q->rear=(Q->rear+1)%MAXSIZE;
    return(TRUE);
}

int GetHead(SeqQueue *Q,int *x)
{
    *x=Q->Element[Q->front];
    return(TRUE);
}

int DeleteQueue(SeqQueue *Q,int *temp)
{
    if(Q->rear==Q->front)
        return(FALSE);
    *temp=Q->Element[Q->front];
    Q->front=(Q->front+1)%MAXSIZE;
    return(TRUE);
}

int IsEmpty(SeqQueue *Q)
{
    if(Q->rear==Q->front)
        return(TRUE);
    return(FALSE);
}

int main()
{
    SeqQueue Q;
    int temp,x,N;
    printf("sum is ");
    scanf("%d",&N);
    InitQueue(&Q);
    EnterQueue(&Q,1);
    for(int n=2;n<=N;n++)
    {
        EnterQueue(&Q,1) ;
        for(int i=1;i<=n-2;i++)
            {
                DeleteQueue(&Q,&temp);
                printf("%d\t",temp);
                GetHead(&Q,&x);
                temp=temp+x;
                EnterQueue(&Q,temp) ;
            }
        DeleteQueue(&Q,&x);
        printf("%d\t",x);
        EnterQueue(&Q,1);
        putchar('\n');
    }
    while(!IsEmpty(&Q))
    {
        DeleteQueue(&Q,&x);
        printf("%d\t",x);
    }
}
