/*现在小学的数学题目也不是那么好玩的。
看看这个寒假作业：

   □ + □ = □
   □ - □ = □
   □ × □ = □
   □ ÷ □ = □

每个方块代表1~13中的某一个数字，但不能重复。
比如：
 6  + 7 = 13
 9  - 8 = 1
 3  * 4 = 12
 10 / 2 = 5

以及： 
 7  + 6 = 13
 9  - 8 = 1
 3  * 4 = 12
 10 / 2 = 5

就算两种解法。（加法，乘法交换律后算不同的方案）

你一共找到了多少种方案？*/

//DFS算法

#include <stdio.h>
#define true 1
#define false 0
typedef int bool;
bool isUsed[14];
int num[4][3];
int sum = 0;

// 判断第几行最后一个值填入这个是否正确 
bool right( int r, int val )
{
    switch(r)
    {
        case 0:
            return num[r][0] + num[r][1] == val;
            break;
        case 1:
            return num[r][0] - num[r][1] == val;
            break;
        case 2:
            return num[r][0] * num[r][1] == val;
            break;
        case 3:
            return val * num[r][1] == num[r][0];
            break;
        default:
            return false;
    }
}

void dfs( int r, int c )
{
    if( r == 4 )
    {
        sum++;
        if( sum )
        {
            for(int i=0;i<4;i++)
            {
                for(int j=0;j<3;j++)
                    printf("%4d", num[i][j]);
                printf("\n");
            }
        }
        printf("\n");
        return;
    }

    for(int i=1;i<=13;i++)
    {
        // 该数没有被使用 
        if( !isUsed[i] )
        {
            isUsed[i] = true;
            // 是计算的结果，则需要判断一下 
            if( c != 2 || right(r, i) )
            {
                num[r][c] = i;
                dfs( r+(c+1)/3, (c+1)%3 );
            }

            isUsed[i] = false;
        }
    }
}

int main()
{
    for(int i=1;i<14;i++)
        isUsed[i] = false;
    dfs( 0, 0 );
    printf("%d\n", sum);
}
