/*把每条边相连，建立一个无向图。如果在一个状态之后遍历这个图，用并查集的方式，
看是否是一个父节点，如果是一个父节点的话，就是相连的*/
#include<bits/stdc++.h>
using namespace std;

int use[10];
int ans,e[10][10],father[10];

void init()
{
    //连边建图
    //a b c d e f g
    //1 2 3 4 5 6 7
    e[1][2] = e[1][6] = 1;
    e[2][1] = e[2][7] = e[2][3] = 1;
    e[3][2] = e[3][4] = e[3][7] = 1;
    e[4][3] = e[4][5] = 1;
    e[5][4] = e[5][6] = e[5][7] = 1;
    e[6][1] = e[6][5] = e[6][7] = 1;
}

int  find(int a)
{
    //并查集
    return (a==father[a])?a:father[a] = find(father[a]);

}
void dfs(int d)
{
    if(d>7)//一个七段管的所有灯的状态已经列举完了
    {
        for(int i=1;i<=7;i++)
        {
            father[i] = i;//注意初始化
        }
        for(int i=1;i<=7;i++)
        {
            for(int j=1;j<=7;j++)
            {
                if(e[i][j] && use[i] && use[j])
                {
                    int fa = find(i),fb = find(j);
                    if(fa != fb)
                    {
                        father[fa] = fb;
                    }
                }
            }
        }

        int k = 0;
        for(int i=1;i<=7;i++)
        {
            if(use[i]&& i == father[i])
                k++;
        }
        if(k==1)//只有一个父节点，就说明他们是相连的
            ans ++;
        return;
    }

    use[d] = 0;//不选
    dfs(d+1);

    use[d]= 1; //选
    dfs(d+1);
    use[d] = 0;//归位
}
int main()
{
    init();
    dfs(1);
    cout<<ans<<endl;
    return 0;
}