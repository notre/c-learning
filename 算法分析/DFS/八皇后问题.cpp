/*八皇后问题是一个以国际象棋为背景的问题：如何能够在 8×8 的国际象棋棋盘上放置八个皇后，
使得任何一个皇后都无法直接吃掉其他的皇后？为了达到此目的，
任两个皇后都不能处于同一条横行、纵行或斜线上.
八皇后问题可以推广为更一般的n皇后摆放问题：
这时棋盘的大小变为n×n，而皇后个数也变成n。当且仅当 n = 1 或 n ≥ 4 时问题有解。*/

/*这个就是定义hang 【num】和列i，然后每一行从第一个位置开始放皇后，
如果可以的话就标记为1，然后接着往下找第二行，也是从第二行第一个位置开始找，
满足接着第三行。当如果发现第三行不能再放置皇后了，这个时候第三行那个位置已经标记为1 ，
所以这个时候就回溯到第二行也就是上一层从新判断同时把标记为1 的变量还原为0，直到满足条件输出。 
题中有一个陷阱，深搜的时候是按列输出的，不是行输出。*/

#include<iostream>
#include<cstdio>
#include<cstdlib>
using namespace std;
int hang[11], n=8;
int a[10][10] = { 0 };
int t = 1;
void print()
{
	printf("No. %d\n", t++);
	for (int i = 1; i <= 8; i++)
	{
		for (int j = 1; j <= 8; j++)
		{
			printf("%d ", a[j][i]);
		}
		printf("\n");
	}
}
bool judge(int num)
{
	for (int i = 1; i < num; i++)
		if (hang[num] == hang[i] || abs(hang[i] - hang[num]) == num - i)
			//判断列和对角线
			return 0;
	return 1;
}
 
void dfs(int num)
{
	if (num >= 9){
		print();
	}
	for (int i = 1; i <= 8; i++)
	{
		hang[num] = i;
		if (a[num][i]!=1&&judge(num))
		{
			a[num][i] = 1;
			dfs(num + 1);
			a[num][i] = 0;
		}
		
	}
	
}
 
int main()
{
	//freopen("1.txt", "w", stdout);
		dfs(1);
	return 0;
}

