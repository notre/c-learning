/*如下的10个格子
   +--+--+--+
   |  |  |  |
+--+--+--+--+
|  |  |  |  |
+--+--+--+--+
|  |  |  |
+--+--+--+

填入0~9的数字。要求：连续的两个数字不能相邻。
（左右、上下、对角都算相邻）

一共有多少种可能的填数方案？ */   

//DFS算法(深度优先搜索)

#include <stdio.h>
#include <string.h>
#define true 1
#define false 0
typedef int bool;

int num[3][4];
int sum = 0;
bool isFilled[10];

int abs(int n)
{
    return n >= 0 ? n : -n;
}

//在num[r][c]处防止i符合规则，否则返回true 
bool isLegal(int r, int c, int val)
{
    if( r == 0 && c == 0 )
        return true;
    else if( r == 0 )
        return abs(val-num[r][c-1]) > 1;
    else if( c == 0 )
        return abs(val-num[r-1][c]) > 1 && abs(val-num[r-1][c+1])>1;
    else if( c != 3 )
        return abs(val-num[r][c-1]) > 1 && abs(val-num[r-1][c]) > 1 && abs(val-num[r-1][c-1])>1 && abs(val-num[r-1][c+1])>1;
    else
        return abs(val-num[r][c-1]) > 1 && abs(val-num[r-1][c]) > 1 && abs(val-num[r-1][c-1])>1;
}

void dfs(int r, int c)
{
    if( r == 2 && c == 3 )
    {
        sum++;
        return;
    }

    for(int i=0;i<=9;i++)
    {
        if( !isFilled[i] && isLegal(r, c, i) )
        {
            num[r][c] = i;
            isFilled[i] = true;
            dfs( r + (c+1)/4, (c+1)%4 );
            isFilled[i] = false;
        }
    }

}

int main()
{
    num[0][0] = -10;
    num[2][3] = -10;
    memset(isFilled,0,sizeof(isFilled));
    dfs(0,1);
    printf("%d\n",sum);
    return 0;      
}
