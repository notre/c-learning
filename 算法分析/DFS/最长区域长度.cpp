#include<iostream>
#include<cstdio>
using namespace std;
 
int R, C;
int map[105][105];
int mark[105][105] = { 0 };
 
 
int dfs(int i, int j)
{
    int k;
    if (mark[i][j]) return mark[i][j];

    if (i != 0 && map[i - 1][j] < map[i][j])
    {
        k = dfs(i - 1, j) + 1;
        if (k> mark[i][j]) mark[i][j] = k;
    }

    if (i != R - 1 && map[i + 1][j] < map[i][j])
    {
        k = dfs(i + 1, j) + 1;
        if (k>mark[i][j]) mark[i][j] = k;
    }

    if (j != 0 && map[i][j - 1]<map[i][j])
    {
        k = dfs(i, j - 1) + 1;
        if (k>mark[i][j]) mark[i][j] = k;
    }

    if (j != C - 1 && map[i][j + 1]<map[i][j])
    {
        k = dfs(i, j + 1) + 1;
        if (k>mark[i][j]) mark[i][j] = k;
    }

    return mark[i][j];
 
}
 
int main()
{
    int i, j, k, sum = 0;
    scanf("%d%d", &R, &C);
    putchar('\n');
    for (i = 0; i < R; i++)
        for (j = 0; j < C; j++)
            scanf("%d", &map[i][j]);
    for (i = 0; i < R; i++)
        for (j = 0; j < C; j++)
            k = dfs(i, j);
            if (k>sum) sum = k;
    putchar('\n');
    cout << sum + 1 << endl;
    return 0;
}

/*这个题就是把每个数从四个方向都遍历一次，如果满足递减的话就接着dfs，不满足时候把这个数存起来 
这个题有几个注意的问题，就是第一个要考虑好边缘临界点，就是四周的点不可以进行某些方向的移动，
其次还有一点特别要注意，
dfs中的if (mark[i][j]) return mark[i][j];这句话就是为了重复计算，
假如从24开始的话已经算出来23，然后如果从25开始，遇到24的话直接可以找到23，
而不用在遍历一次，节省了时间。 */