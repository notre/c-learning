#include<stdio.h>

void move(char x,char y)
{
	printf("%c-->%c\n",x,y);
}

void hannoi(int n,char one,char two,char three)
{
	if(n==1)
		move(one,three);
	else
	{
		hannoi(n-1,one,three,two);  //把A盘上的n-1个盘子借助C盘移动到B盘
		move(one,three);
		hannoi(n-1,two,one,three);
	}
}

int main(void)
{
    int n;
    printf("sum is:");
    scanf("%d",&n);
    hannoi(n,'A','B','C');
    return 0;
}