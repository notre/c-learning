#include<stdio.h>
#include<string.h>
#define MAXSIZE 100
#define R 5
#define C 5
typedef struct 
{
    int row,col;
    int e;
}Triple;

typedef struct 
{
    Triple data[MAXSIZE+1];
    int m,n,len;
}TSMatrix;

void print(TSMatrix A)
{
    printf("  row  col  e\n");
    for(int i=1;i<=A.len;i++)
    {
        printf("%4d,%9d,%13d\n",A.data[i].row,A.data[i].col,A.data[i].e);
    }
}

void TransposeTSMatrix(TSMatrix A,TSMatrix *B)
{
    int i,j,k=1;
    B->m=A.n,B->n=A.m,B->len=A.len;
    if(B->len>0)
    {
        for(i=1;i<=B->len;i++)
        for(j=1;j<=B->len;j++)
            if(A.data[j].col==i)
            {
                B->data[k].row=A.data[j].col;
                B->data[k].col=A.data[j].row;
                B->data[k].e=A.data[j].e;
                k++;
            }
    }
}

void FastTransposeTSMatrix(TSMatrix A,TSMatrix *B)
{
    int i,j,k,q,p;
    int num[MAXSIZE],position[MAXSIZE];
    B->m=A.n,B->n=A.m,B->len=A.len;
    if(B->len)
    {
        memset(num,0,sizeof(num));
        for(i=1;i<=A.len;i++)
            num[A.data[i].col]++;
        position[1]=1;
        for(j=2;j<=A.len;j++)
            position[j]=position[j-1]+num[j-1];
        for(k=1;k<=A.len;k++)
        {
            p=A.data[k].col,q=position[p];
            B->data[q].col=A.data[k].row;
            B->data[q].row=A.data[k].col;
            position[p]++;
        }
    }
}

TSMatrix creat(TSMatrix A,int a[R][C],TSMatrix B)
{
    A.m=R;
    A.n=C;
    A.len=0;
    int k,j;
    printf("print %d*%d juzhen ",A.m,A.n);
    for(k=0;k<A.m;k++)
        {
            printf("di %d hang:",k+1);
            for(j=0;j<A.n;j++)
            {  
                scanf("%d",&a[k][j]);
            }    
        }

    printf("\nxi shu ju zhen :\n");
    for(k=0;k<A.m;k++)
        {
            for(j=0;j<A.n;j++)
            {  
                if(a[k][j]!=0)
                {
                    A.len++;
                    A.data[A.len].row=k+1;
                    A.data[A.len].col=j+1;
                    A.data[A.len].e=a[k][j];
                }
                printf("\t%d",a[k][j]);
            }   
            putchar('\n'); 
        }
    
    if(A.len<R*C*0.3)
    {
        printf("xishujuzhen have %d ge yuansu >0\n",A.len);
        printf("san yuan zu :\n");
        print(A);

        printf("lei zhuan zhi:\n");
        TransposeTSMatrix(A,&B);
        print(B);

        printf("yici kuaisu zhuan zhi:\n");
        FastTransposeTSMatrix(A,&B);
        print(B);
    }
    else
        printf("bu shi xi shu ju zhen!!!");
}

int main()
{
    TSMatrix A;
    TSMatrix B;
    int a[R][C],i=1; 
    creat(A,a,B);
    return 0;
}