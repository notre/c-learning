#include<stdio.h>
#define N 8

int main(void)
{
    int i,j ,a[N+1][N+1];
    a[N][N]=0;
    for(i=1;i<=N;i++){
        for(j=1;j<=N;j++){
            if(i%4==j%4||(i%4+j%4)==1||(i%4+j%4)==5){
                a[i][j]=(N+1-i)*N-j+1;
            }
            else{
                a[i][j]=(i-1)*N+j;
            }
        }
    }
    for(i=1;i<=N;i++){
        for(j=1;j<=N;j++){
            printf("%4d",a[i][j]);
            }
        printf("\n");
    }
    return 0;
}