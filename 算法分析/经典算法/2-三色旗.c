#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#define BLUE 'b'
#define WHITE 'w'
#define RED 'r'

#define swap(x,y)   {char temp;temp=color[x],color[x]=color[y];color[y]=temp;}

int main()
{
    char color[]={'r','w','b','w','w','b','r','b','w','r','\0'};
    int wFlag=0,bFlag=0,rFlag=strlen(color)-1;
    int i,count=0;

    for(i=0;i<strlen(color);i++)
        printf("%c\t",color[i]);
    printf("\n\n");

    while(wFlag<=rFlag)
    {
        if(color[wFlag]==WHITE)
        {
            wFlag++;
        }
        else if(color[wFlag]==BLUE)
        {
            swap(bFlag,wFlag);
            bFlag++,wFlag++;
        }
        else{
            while (wFlag<rFlag&&color[rFlag]==RED)
            {
                rFlag--;
            }
            swap(rFlag,wFlag);
            rFlag--;
        }
        
        for(i=0;i<strlen(color);i++)
            printf("%c\t",color[i]);
        printf("\n");
        count++;
    }
    printf("%d",count);
    return 0;
}