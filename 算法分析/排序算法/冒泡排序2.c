#include <stdio.h>
#include <stdlib.h>
#include <time.h>
int main()
{
    int a[10],i,j,temp;
    srand((unsigned int)time(NULL));
    for(i = 0;i<10;i++)
        a[i] = rand()%100+1;
    for(i = 0;i<9;i++)
      for(j = 0;j<9-i;j++)
      {
          if(a[j]<a[j+1])
          {
              temp = a[j];
              a[j] = a[j+1];
              a[j+1] = temp;
          }
      }
    for(i = 0;i<10;i++)
      printf("%d ",a[i]);
    return 0;
}
