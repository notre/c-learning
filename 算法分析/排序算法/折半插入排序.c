#include<stdio.h>
#include<string.h>
#define len 5

void binarySort(int *a)
{
    int i,j,low,mid,high,temp;
    for(i=1;i<len;i++)
    {
        low=0;
        high=i-1;
        temp=a[i];
        while(low<=high)
        {
            mid=low+(high-low)/2;
            if(temp<a[mid])
                high=mid-1;
            else    low=mid+1;
        }
        for(j=i-1;j>=high+1;j--)
        {
            a[j+1]=a[j];
        }
        a[high+1]=temp;
    }
}

int main()
{
    int a[]={45,32,56,71,12};
    int i;
    printf("one\t");
    for(i=0;i<len;i++)
    {
        printf("%d\t",a[i]);
    }
    printf("\n");
    binarySort(a);
    printf("tow\t");
    for(i=0;i<len;i++)
    {
        printf("%d\t",a[i]);
    }
}