//求分子分母都在1~2021之间的既约分数
//辗转相除法
#include<stdio.h>
int sum;

int gcd(int x,int y)
{
    int temp;
    if(x<y)
    {
        temp=x;
        x=y;
        y=temp;
    }
    if(x%y==0)
    {
        return y;
    }
    else
    {
        return(gcd(y,x%y));
    }
}

int main()
{
    for(int i=1;i<=2021;i++)
    {
        for(int j=1;j<=2021;j++)
        {
            if(gcd(i,j)==1) sum++;
        }
    }
    printf("%d",sum);
    return 0;
}