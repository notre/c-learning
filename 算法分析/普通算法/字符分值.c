//求出一个字符串中恰好出现一次的字符个数，字符串在i~j中任意截取长度
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
int s[26];
int sum=0;

void ww(char a[],int x,int y)
{
    memset(s,0,sizeof(s));
    for(int i=x;i<=y;i++)
    {
        s[a[i]-'a']++;
    }
    for(int j=0;j<26;j++)
    {
        if(s[j]==1) sum++;
    }  
}

int main()
{
    char a[100];
    int k=0;
    printf("请输入字符串：");
    gets(a);
    k=strlen(a);
    /*while(a[k]!='\0')
    {
        k++;
    }*/
    for(int i=0;i<k;i++)
    {
        for(int j=i;j<k;j++)
        {
            ww(a,i,j);
        }
    }
    printf("sum is %d !\n",sum);
    return 0;
}
