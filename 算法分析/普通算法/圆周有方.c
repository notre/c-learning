#include<stdio.h>
#include<math.h>
#include<time.h>
#include<stdlib.h>
#define N 3000
void main()
{
    double e=0.1,b=0.5,c,d;
    long int i;
    float x,y;
    int c2=0,d2=0;
    puts("******************************************");
    for(i=6;;i*=2)          //正多边形边数加倍
    {
        d=1.0-sqrt(1.0-b*b);        //计算圆内接正多边形的边长
        b=0.5*sqrt(b*b+d*d);        
        if(2*i*b-i*e<1e-15) break;
    }
    printf("pi=%.15lf\n",2*i*b);
    printf("%ld\n",i);

    while (c2++<N)
    {
        x=rand()%101;
        y=rand()%101;
        if(x*x+y*y<=10000)
            d2++;
    }
    printf("-----------------------------------------\n");
    printf("pi=%f\n",4.*d2/N);
}