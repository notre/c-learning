#include<stdio.h>
#include<stdlib.h>
#define M 10
typedef struct {
	int data[M];
	int len;
}Seqlist;
typedef struct LNode{
	int data;
	struct LNode *next;
}LNode,*Linklist;

void InitSeqlist(Seqlist *L);
void PrintList(Seqlist *L);
void DeleteSame(Seqlist *L);

int main() {
	Seqlist List;
	Seqlist *L = &List;
	InitSeqlist(L);
	printf("初始顺序表\n");
	PrintList(L);
	DeleteSame(L);
	printf("删除删除顺序表中重复元素\n");
	PrintList(L);
}
void DeleteSame(Seqlist *L)
{
	int p = L->len, k = 0;
	LNode *H[M], *S;//H[M]为散列表的数组部分
	for (int i = 0; i < p; i++)
		H[i] = NULL;
	for (int i = 0, j; i < L->len; i++)
	{
		j = L->data[i] % p;//j为元素对应的散列表数组单元
		if (H[j] == NULL)//判断对应位置链表是否为空
		{
			S = (LNode*)malloc(sizeof(LNode));//创建同义词节点
			S->data = L->data[i];
			S->next = NULL;
			H[j] = S;
			L->data[k++] = L->data[i];//将与元素放入新表中
		}
		else//对应位置链表不为空
		{
			S = H[j];
			while(S != NULL)//判断同义词链表中是否有相同元素
			{
				if (S->data == L->data[i])
					break;
				S = S->next;
			}
			if (S == NULL)//没有相同元素
			{
				S = (LNode*)malloc(sizeof(LNode)); // 创建同义词节点
				S->data = L->data[i];
				S->next = H[j];
				H[j] = S;
				L->data[k++] = L->data[i];//将与元素放入新表中
			}
		}
	}
	L->len = k;
}
void InitSeqlist(Seqlist *L)//初始化链表
{
	int n = 10;
	int a[10] = { 25,25,25,15,8,3,8,18,13,3 };//测试用顺序表
	for (int i = 0; i < n; i++) 
	{
		L->data[i] = a[i];
	}
	L->len = 10;
}
void PrintList(Seqlist *L)//打印链表
{
	for (int i = 0; i < L->len; i++)
	{
		printf("%d ", L->data[i]);
	}
	printf("\n");
}
