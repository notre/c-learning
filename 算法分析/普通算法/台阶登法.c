//总共39级台阶，每步可以迈一级或两级台阶，先迈左脚，然后左右交替，最后迈右脚，所以总共是偶数步，有多少种上法
#include<stdio.h>
int sum=0;

int ans(int n,int step)
{
    if(n<0) return 0;
    if(n==0&&step%2==0)
    {
        sum++;
    }
    else    
    {
        ans(n-1,step+1);
        ans(n-2,step+1);
    }
}

int main()
{
    ans(39,0);
    printf("%d",sum);
    return 0;
}
