//给定数字1,2,3,4，可以组成多少无重复数字的三位数
#include<stdio.h>
int main()
{
    int i,j,k;
    int m=0;
    int a[100][3]={};
    for(i=1;i<5;i++)
        for(j=1;j<5;j++)
            for(k=1;k<5;k++)
            {
                a[m][0]=i;
                a[m][1]=j;
                a[m][2]=k;
                m++;
            }
    
    for(int c=0;c<m;c++)
    {
        if(a[c][0]==a[c][1]||a[c][0]==a[c][2]||a[c][1]==a[c][2])
            a[c][0]=0;
    }

    printf("sum is \n");
    for(int n=0;n<m;n++)
    {
        if(a[n][0]!=0)
        printf("%d%d%d\n",a[n][0],a[n][1],a[n][2]);
    }
    return 0;
}
