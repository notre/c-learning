//删除删除顺序表中重复元素
#include<stdio.h>
#include<stdint.h>
#define M 100
typedef struct
{
    int data[M];
    int last;
}Seqlist;
int main()
{
    Seqlist L;
    initlist(&L);
    printf("sum is one \n");
    printlist(&L);
    dellist(&L);
    printf("\nsum is two \n");
    printlist(&L);
    return 0;
}

void initlist(Seqlist *L)
{
    int a[10]={8,6,9,9,2,9,9,4,7,2};
    for(int i=0;i<10;i++)
    {
        L->data[i]=a[i];
    }
    L->last=10;
}

void printlist(Seqlist *L)
{
    for(int i=0;i<L->last;i++)
    {
        printf("%d",L->data[i]);
    }
}

void dellist(Seqlist *L)
{
    for(int i=0;i<L->last;i++)
    {
        for(int k=i+1;k<L->last;k++)
        {
            if(L->data[i]!=L->data[k])
                continue;
            else
            {
                for(int m=k;m<L->last;m++)
                {
                    L->data[m]=L->data[m+1]; 
                }
                L->last--;
            }
            
        }
    }

}