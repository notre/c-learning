#include<stdio.h>
#define Stack_Size 100
#define StackElementType int
#define FALSE 0
#define TRUE 1
typedef struct
{
    StackElementType elem[Stack_Size];
    int top;
}SeqStack;

void InitStack(SeqStack *S)
{
    S->top=-1;
}

int Push(SeqStack *S,StackElementType x)
{
    if(S->top==Stack_Size-1)    return(FALSE);
    else
        S->elem[++(S->top)]=x;
        return(TRUE);
}

int Pop(SeqStack *S,StackElementType x)
{
    if(S->top==-1)
        return(FALSE);
    else
    {
        x=S->elem[(S->top)--];
        return(x);
    }
}

int main()
{
    SeqStack S;
    InitStack(&S);
    char a[100],e,x,flag,p;
    int i=0;
    printf("e is ");
    scanf("%c",&e);
    a[i]=e,i++;
    while(e!='@')
    {
        Push(&S,e);
        printf("e is ");
        scanf(" %c",&e);
        a[i]=e,i++;  
    }
    for(int n=0;n<i-1;n++)
    {
        flag=Pop(&S,x);
        if(flag==a[n]) 
            p=1;
        else
            p=0;
            break;     
    }
    if(p==1)
        printf("Yes\n");
    else
        printf("NO\n");
    return 0;
}