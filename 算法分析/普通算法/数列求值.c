//1、1、1、3、5、9、17···第四项开始都是前面三项的和，求20211004后四位
#include<stdio.h>
int main()
{
    long long a=1,b=1,c=1,d;
    for(int i=4;i<=20211004;i++)
    {
        d=(a+b+c)%10000;
        a=b,b=c,c=d;
    }
    printf("%lld",d);
    return 0;
}