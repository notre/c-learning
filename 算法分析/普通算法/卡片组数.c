/*小蓝有很多数字卡片，每张卡片上都是数字 0 0 0 到 9 9 9。
小蓝准备用这些卡片来拼一些数，他想从 1 1 1 开始拼出正整数，每拼一个，就保存起来，
卡片就不能用来拼其它数了。小蓝想知道自己能从 1 1 1 拼到多少。
例如，当小蓝有 30 30 30 张卡片，其中 0 0 0 到 9 9 9 各 3 3 3 张，则小蓝可以
拼出 1 1 1 到 10 10 10，但是拼 11 11 11 时卡片 1 1 1 已经只有一张了，不够拼出 11 11 11。*/

#include<stdio.h>
#define true 1
#define false 0
int a[10];
int fun(int n)
{
    while (n)
    {
        if(a[n%10]>0)
            --a[n%10];
        else
            return false;
        n/=10;
    }
    return true; 
}

int main()
{
    int p=1;
    for(int i=0;i<=9;i++)
    {
        a[i]=2021;
    }
    while(true)
    {
        if(!fun(p)) break; 
        p++;
    }
    printf("%d",p-1);
    return 0;
}