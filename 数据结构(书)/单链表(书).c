//单链表的存储结构描述
typedef struct Node
{
    ElemType data;
    struct Node * next;
}Node,*LinkList;

//初始化单链表
InitList(LinkList L)
{
    *L=(LinkList)malloc(sizeof(Node));
    (*L)->next=NULL;
}

//用头插法建立单链表
void CreateFromHead(LinkList L)
{
    Node *s;
    char c;
    int flag=1;
    while(flag)
    {
        c=getchar();
        if(c!='$')
            {
                s=(Node *)malloc(sizeof(Node));
                s->data=c;
                s->next=L->next;
                L->next=s;
            }
        else flag=0;
    }
}

//尾插法建立单链表
void CreatFromTail(LinkList L)
{
    Node *r,*s;
    int flag=1;
    r=L;
    while
    {
        c=getchar();
        if(c!='$')
        {
            s=(Node *)malloc(sizeof(Node));
            s->data=c;
            r->next=s;
            r=s;
        }
        else
        {
            flag=0;
            r->next=NULL;
        }
    }
}

//在单链表L中查找第i个节点
Node * Get(LinkList L,int i)
{
    int j;
    Node *p;
    if(i<=0)    return NULL;
    p=L;j=0;
    while((p->next!=NULL)&&(j<i))
    {
        p=p->next;
        j++;
    }
    if(i=j) return p;
    else return NULL;
}//时间复杂度为O(n)

//按值查找
Node * Locate(LinkList L,ElemType key)
{
    Node *p;
    p=L->next;
    while(p!=NULL)
        if(p->data!=key)
            p=p->next;
        else break;
    return p;
}//时间复杂度为O(n)

//求单链表长度
int ListLenght(LinkList L);
{
    Node *p;
    p=L->next;
    j=0;
    while(p!=NULL)
    {
        p=p->next;
        j++;
    }
    return j;
}

//单链表插入操作
void InsList(LinkList L,int i,ElemType e)
{
    Node *pre,*s;
    int k;
    if(i<=0) return ERROR;
    pre=L;  k=0;
    while(pre!=NULL&&k<i-1)
    {
        pre=pre->next;
        k=k+1;
    }
    if(pre=NULL)
    {
        printf("插入位置不合理！");
        return ERROR;
    }   
    s=(Node * )malloc(sizeof(Node));
    s->data=e;
    s->next=pre->next;
    pre->next=s;
    return OK;
}

//单链表的删除操作
int DelList(LinkList L,int i,ElemType *e)
{
    Node *pre,*r;
    int k;
    pre=L;k=0;
    while(pre->next!=NULL&&k<i-1)
    {
        pre=pre->next;
        k++;
    }
    if(pre-next==NULL)
    {
        printf("删除节点位置i不合理！")
        return ERROR;
    }
    r=pre->next;
    pre->next=r->next;
    *e=r->data;
    free(r);
    return OK;
}

//合并两个有序的单链表
LinkList MergeLinkList(LinkList LA,LinkList LB)
{
    Node *pa,*pb;
    LinkList LC;
    pa=LA->next;
    pb=LB->next;
    LC=LA;
    LC->next=NULL;r=LC;
    while(pa!=NUll&&pb!=NULL)
    {
        if(pa->data<pb->data)
            r->next=pa;r=pa;pa=pa->next;
        else
            r->next=pb;r=pb;pb=pb-next;
    }
    if(pa)
        r-next=pa;
    else
        r->next=pb;
    free(LB);
    return(LC);
}