//二叉树的链式存储结构
typedef struct Node
{
    DataType data;
    struct Node *LChild;    //左子树
    struct Node *RChild;    //右子树
}BiTNode,*BiTree;

//先序遍历
void PreOrder(BiTree root)
{
    if(root!=NULL)
    {
        Visit(root->data);
        PreOrder(root->LChild);
        PreOrder(root->RChild);
    }
}

//中序遍历
void InOrder(BiTree root)
{
    if(root!=NULL)
    {
        InOrder(root->LChild);
        Visit(root->data);
        InOrder(root->RChild);
    }
}

//后序遍历
void PostOrder(BiTree root)
{
    if(root!=NULL)
    {
        PostOrder(root->LChild);
        PostOrder(root->RChild);
        Visit(root->data);
    }
}

//先序遍历输出二叉树中的结点
void PreOrder(BiTree root)
{
    if(root!=NULL)
    {
        printf(root->data);
        PreOrder(root->LChild);
        PreOrder(root->RChild);
    }
}

//先序遍历输出二叉树中的叶子结点
void PreOrder(BiTree root)
{
    if(root!=NULL)
    {
        if(root->LChild==NULL&&root->RChild==NULL)
            printf("root->data");
        PreOrder(root->LChild);
        PreOrder(root->RChild);
    }
}

//后序遍历统计叶子节点数目
void leaf(BiTree root)
{
    if(root!=NULL)
    {
        leaf(root->LChild);
        leaf(root->RChild);
        if(root->LChild==NULL&&root->RChild==NULL);
            LeafCount++;
    }
}

//后序遍历统计叶子节点数目（分治思想）
int leaf(BiTree root)
{
    int LeafCount;
    if(root==NULL)
        LeafCount=0;
    else if((root->LChild==NULL)&&(root->RChild==NULL))
        LeafCount=1;
    else
        LeafCount=leaf(root->LChild)+leaf(root->RChild);
    return LeafCount;
}

//建立二叉链表方式存储的二叉树
//用扩展先序遍历序列创建二叉链表
void CreateBiTree(BiTree *bt)
{
    char ch;
    ch=gerchar();
    if(ch== '.')    *bt=NULL;
    else{
        *bt=(BiTree)malloc(sizeof(BiTNode));
        (*bt)->data=ch;
        CreatBiTree(&((*bt)->LChild));
        CreatBiTree(&((*bt)->RChild));
    }
}

//求二叉树的高度
//后序遍历求二叉树高度的递归算法
int PostTreeDepth(BiTree bt)
{
    int hl,hr,max;
    if(bt!=NULL){
        hl=PostTreeDepth(bt->LChild);
        hr=PostTreeDepth(bt->rChild);
        max=hl>hr?hl:hr;
        return(max+1);
    }
    else return(0);
}

//先序遍历求二叉树高度的递归算法
void PreTreeDepth(BiTree bt,int h)  //h为bt指向结点所在层次，初值为1
{                                   //depth为当前求得的最大层次，为全局变量，调用前初值为0
    if(bt!=NULL){
        if(h>depth) depth=h;
        PreTreeDepth(bt->LChild,h+1);
        PreTreeDepth(bt->RChild,h+1);
    }
}

//按树状打印二叉树
void PrintTree(BiTree bt,int nLayer)    //nLayer为层数
{
    if(bt==NULL)    return;
    PrintTree(bt->RChild,nLayer+1);
    for(int i=0;i<nLayer;i++)
        printf(" ");
    printf("%c\n",bt->data);
    PrintTree(bt->LChild,nLayer+1);
}

（基于栈的递归消除）
//（a）中序遍历二叉树非递归算法初步
void inorder(BiTree root)
{
    int top=0;p=root;
    L1:if(p!=NULL)          //遍历左子树
    {
        top=top+2;
        if(top>m)   return; //栈满溢出
        s[top-1]=p;         //本层参数进栈
        s[top]=L2;          //返回地址进栈
        p=p->LCild;         //给下层参数赋值
        goto L1;            //转向开始
      L2:Visit(p->data); //访问根
        top=top+2;
        if(top>m)   return; //栈满
        s[top-1]=p;
        s[top]=L3;
        p=p->RChild;
        goto L1;
    }
    L3:if(top!=0)
    {
        addr=s[top];
        p=s[top-1];         //取出返回地址
        top=top-2;          //退出本层参数
        goto addr;
    }
}

//（b）中序遍历二叉树的非递归算法（直接实现栈操作）
void inorder(BiTree root)       //s[m]表示栈，top表示栈顶指针
{
    top=0;p=root;
    do{
        while(p!=NULL){
            if(top>m)   return;
            top=top+1;
            s[top]=p;
            p=p->LChild;
        }   //遍历左子树
        if(top!=0){
            p=s[top];
            top=top-1;
            Visit(p->data);     //访问根节点
            p=p->RChild;        //遍历右子树
        }
    }while(p!=NULL||top!=0)
}

//（c）中序遍历二叉树的非递归算法（调用栈操作的函数）
void InOrder(BiTree root)
{
    InitStack(&S);
    p=root;
    while(p!=NULL||!IsEmpty(S)){        //IsEmpty判断栈空函数，为空返回true，不为空返回false
        if(p!=NULL){                    //根指针进栈，遍历左子树
            Push(&S,p);
            p=p->LChild;
        }
        else{                           //根指针退栈，访问根节点，遍历右子树
            Pop(&S,&p);Visit(p->data);
            p=p->RChild;
        }
    }
}

//后序遍历二叉树的非递归算法（调用栈操作的函数）
void PostOrder(BiTree root)
{
    BiTNode *p,*q;
    Stack S;
    q=NULL;
    p=root;
    InitStack(&S);
    while(p!=NULL||!IsEmpty(S)){
        if(p!=NULL){
            Push(&S,p);p=p->LChild;
        }
        else{
            GetTop(&S,&p);
            if((p->RChild==NULL)||(p->RChild==q)){
                visit(p->data);
                q=p;                    //保存到q，为下一次已处理结点前驱
                Pop(&S,&p);
                p=NULL;
            }
            else
                p=p->RChild;
        }
    }
}

（线索二叉树）
//建立中序线索树
void Inthread(BiTree root)
{
    if(root!=NULL)
    {
        Inthread(root->LChild);             //线索化左子树
        if(root->LChild==NULL)              
        {root->Ltag=1;root->LChild==NULL;}  //置前驱线索
        if(pre!=NULL&&pre->RChild==NULL)    //置后继线索
        {pre->RChild=root;pre->Rtag=1;}     
        pre=root;                           //当前访问结点为下一节点的前驱
        Inthread(root->RChild)              //线索化右子树
    }
}

//在中序线索树中找结点前驱
BiTNode *InPre(BiTNode *p)
{
    if(p->Ltag==1)  pre=p->LChild;
    else{   //在p的左子树中查找最右下方结点
        for(q=p->LChild;q->Rtag==0;q=q->RChild);    //Rtag==0,是右子树不为空时
        pre=q;
    }
    return(pre);
}

//在中序线索树中找结点后继
BiTNode *InNext(BiTNode *p)
{
    if(p->Rtag==1)  Next=p->RChild;
    else{   //在p的右子树中查找最左下方结点
        for(q=p->RChild;q->Ltag==0;q=q->LChild);    //Rtag==0,是左子树不为空时
        Next=q;
    }
    return(Next);
}

//在中序线索树上求中序遍历的第一个结点
BiTNode *InFirst(BiTree Bt)
{
    BiTNode *p=Bt;
    if(!p)  return(NULL);
    while(p->LTag==0)   p=p->Lchild;
    return p;
}

//遍历中序二叉线索树
void TInOrder(BiTree Bt)
{
    BITNode *p;
    p=InFirst(Bt);
    while(p){
        visit(p);
        p=InNext(p);
    }
}

（树的存储结构）
//双亲表示法形式说明
#define MAX 100
typedef struct TNode
{
    DataType data;
    int parent;
}TNode;

typedef struct            //树定义
{
    TNode tree[MAX];
    int nodenum;
}ParentTree;

//孩子表示法的存储结构
typedef struct ChildNode        //孩子链表节点的定义
{
    int Child;
    struct ChildNode *next;
}ChildNode;

typedef struct                  //顺序表节点的结构定义
{
    DataType data;
    ChildNode *FirstChild;      //指向孩子链表的头指针
}DataNode;

typedef struct                  //树的定义
{
    DataNode nodes[MAX];        //顺序表
    int root;                   //该树根结点在线性表中的位置
    int num;                    //树结点的个数
}ChildTree;

//孩子兄弟表示法
typedef struct 
{
    DataType data;              //结点信息
    Struct CSNode *FirstChild;  //第一个孩子
    Struct CSNode *Nextsibling; //下一个兄弟
};

（树，森林和二叉树的相互转换）

//树的遍历算法的实现（法一）
void RootFirst(CSTree root)
{
    if(root!=NULL){
        Visit(root->data);
        p=root->FirstChild;
        while(p!=NULL){
            RootFirst(p);       //访问以p为根的子树
            p=p->Nextsibling;
        }
    }
}

//树的遍历算法的实现（法二）
void RootFirst(CSTree root)
{
    if(root!=NULL){
        Visit(root->data);
        RootFirst(root->FirstChild);    //先根遍历首子树
        RootFirst(root->Nextsibling);   //先根遍历兄弟树
    }
}