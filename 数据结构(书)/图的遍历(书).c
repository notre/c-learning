/*图g：
    A→→→→→B
    ↓ ↖
    ↓   ↖
    C→→→→→D
*/

（深度优先遍历）
//深度优先遍历图g
#define True 1
#define False 0
#define Error -1                    //出错
#define OK 1
int visited[MAX_VERTEX_NUM];        //访问标志数组

void Traverse(Graph g)
{
    for(vi=0;vi<g.vexnum;vi++)  visited[vi]=False;  //初始化
    for(vi=0;vi<g.vexnum;vi++)
        if(!visited[vi])    DFS(g,vi);
}

//深度优先遍历v0所在的联通子图
void DFS(Graph ,int v0)
{
    visit(v0);visited[v0]=True;
    w=FirstAdjVertex(g,v0);
    while(w!=-1){
        if(!visited[w]) DFS(g,w);
        w=NextAdjVertex(g,v0,w);
    }
}

//采用邻接矩阵方式实现深度优先搜索
void DFS(AdjMatrix g,int v0)
{
    visit(v0);visited[v0]=True;
    for(vj=0;vj<g.vernum;vj++)
        if(!visited[vj]&&g.arcs[v0][vj].adj==1)
            DFS(g,vj);
}

//采用邻接表方式实现深度优先搜索
void DFS(AdjMatrix g,int v0)
{
    visit(v0);visited[v0]=True;
    p=g.vertex[v0].firstarc;
    while(p!=NULL){
        if(!visited[p->adjvex])    DFS(g,p->adjvex);
        p=p->nextarc;
    }
}

//非递归形式的DFS
void DFS(Graph g,int v0)
{
    InitStack(&S);
    Push(&S,v0);
    while(!IsEmpty(S)){
        Pop(&S,&v);
        if(!visited[v]){                //栈中可能有重复顶点
            visit(v);
            visited[v]=True;
            w=FirstAdjVertex(g,v);      //求v的第一个邻接点
            while(w!=-1){
                if(!visited[w]) Push(&S,w);
                w=NextAdjVertex(g,v,w);         //求v相对于w的下一个邻接点
            }
        }
    }
}

（广度优先遍历）
//广度优先搜索图g中v0所在的连通子图BFS
void BFS(Graph g,int v0)
{
    visit(v0);  visited[v0]=True;
    InitQueue(&Q);
    EnterQueue(&Q,v0);              //v0进队
    while(!Empty(Q)){
        DeleteQueue(&Q,&v);         //队头元素出队
        w=FirstAdjVertex(g,v);      //求v的第一个邻接点
        while(w!=-1){
            if(!visited[w]){
                visit(w);   visited[w]=True;
                EnterQueue(&Q,w);
            }
            w=NextAdjVertex(g,v,w)  //求v的相对于w的下一个邻接点
        }
    }
}