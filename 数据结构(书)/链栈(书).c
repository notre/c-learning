//结构定义
typedef struct node
{
    StackElementType data;
    struct node *next;
}LinkStackNode;
typedef LinkStackNode *LinkStack;

//初始化
void InitStack(LinkStack top)
{
    top->next=NULL;
}

//进栈
int Push(LinkStack top,StackElementType x)
{
    LinkStackNode *temp;
    temp=(LinkStack*)malloc(sizeof(LinkStack));
    if(temp==NULL)  return(FALSE);
    temp->data=x;
    temp->next=top->next;
    top->next=temp;
    return(TRUE);
}

//出栈
int Pop(LinkStack top,StackElementType *e)
{
    LinkStackNode *temp;
    temp=top->next;
    if(temp==NULL)  return(FALSE);
    top->next=temp->next;
    *x=temp->data;
    free(temp);
    return(TRUE);
}

//多栈运算
#define M 10
typedef struct node
{
    StackElementType data;
    struct node *next;
}LinkStackNode,*LinkStack;
LinkStack top[M];