    （链队列）
//定义
typedef struct Node
{
    QueueElementType data;
    struct Node *next;
}LinkQueueNode;

typedef struct
{
    LinkQueueNode *front;   //队头（出元素）
    LinkQueueNode *rear;    //队尾（入元素）
}LinkQueue;

//初始化
int InitQueue(LinkQueue *Q)
{
    Q->front=(LinkQueueNode*)malloc(sizeof(LinkQueueNode));
    if(Q->front!=NULL);
    {
        Q->rear=Q->front;
        Q->front->next=NULL;
        return(TRUE);
    }
    else return(FALSE);
}

//入队
int EnterQueue(LinkQueue *Q,QueueElementType x)
{
    LinkQueueNode *NewNode;
    NewNode=(LinkQueueNode*)malloc(sizeof(LinkQueueNode));
    if(NewNode!=NULL)
    {
        NewNode->data=x;
        NewNode->next=NULL;
        Q->rear->next=NewNode;
        Q->rear=NewNode;
        return(TRUE);
    }
    else return(FALSE);
}

//出队
int DeleteQueue(LinkQueue *Q,QueueElementType *x)
{
    LinkQueueNode *p;
    if(Q->front==Q->rear)
        return(FALSE);
    p=Q->front->next;
    Q->front->next=p->next;
    if(Q->rear==p)
        Q->rear=Q->front;
    *x=p->data;
    free(p);
    return(TRUE);
}


（循环队列）
//定义
#define MAXSIZE 50
typedef struct
{
   QueueElementType element[MAXSIZE];
   int front;
   int rear;
}SeqQueue;

//初始化
void InitQueue(SeqQueue *Q)
{
   Q->front=Q->rear=0;
}

//入队
int EnterQueue(SeqQueue *Q,QueueElementType x)
{
   if((Q->rear+1)%MAXSIZE==Q->front)   //尾指针加1追上头指针,标志队列已经满了
      return(FALSE);
   Q->element[Q->rear]=x;
   Q->rear=(Q->rear+1)%MAXSIZE;  //重新设置队尾指针
   return(TRUE);
}

int DeleteQueue(SeqQueue *Q,QueueElementType *x)
{
   if(Q->front==Q->rear)   //队空
      return(FALSE);
   *x=Q->element[Q->front];
   Q->front=(Q->front+1)%MAXSIZE;   //重新设置队头指针
   return(TRUE);
}
