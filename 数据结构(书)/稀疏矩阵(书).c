//稀疏矩阵三元组表类型定义
#define MAXSIZE 1000
typedef struct
{
    int row,col;
    ElementType e;
}Triple;
typedef struct
{
    Triple data[MAXSIZE+1];     //三元组表，data[0]未用
    int m,n,len;                //矩阵的m行数、n列数、非零元素个数
}TSMatrix;

//矩阵转置经典算法
void TransMatrix(ElementType source[m][n],ElementType dest[n][m])
{                   //source 和 dest 分别为被转置的矩阵和转置以后的矩阵(矩阵用二维数组表示）
    int i,j;
    for(i=0;i<m;i++)
        for(j=0;j<n;j++)
            dest[j][i]=source[i][j];
}

//列序递增转置法(时间复杂度为O(A.n×A.len))
void TransposeTSMatrix(TSMatrix A,TSMatrix *B)
{                   //把矩阵A转置到B所指向的矩阵中去（矩阵用三元组表示）
    int i,j,k;
    B->m=A.n;B->n=A.m;B->len=A.len;
    if(B->len>0)
    {
        j=1;
        for(k=1;k<=A.n;k++)
            for(i=1;i<=A.len;i++)
                if(A.data[i].col==k)
                {
                    B->data[j].row=A.data[i].col;
                    B->data[j].col=A.data[i].row;
                    B->data[j].e=A.data[i].e;
                    j++;
                }
    } 
}

//选票统计问题
for(i=1;i<=20000;i++)
{
    scanf("%d",&x);
    count[x]++;
}

//稀疏矩阵一次定位快速转置法(时间复杂度为O(A.n+A.len))
FastTransposeTSMatrix(TSMatrix A,TSMatrix *B)
{
    int col,t,p,q;
    int num[MAXSIZE],position[MAXSIZE];
    B->len=A.len;B->n=A.m;B->m=A.n;
    if(B->len)
    {
        for(col=1;col<=A.n;col++)
            num[col]=0;
        for(t=1;t<=A.len;t++)
            num[A.data[t].col]++;
        position[1]=1;
        for(col=2;col<=A.n;col++)
            position[col]=position[col-1]+num[col-1];
        for(p=1;p<=A.len;p++)
        {
            col=A.data[p].col;  q=position[col];
            B->data[q].row=A.data[p].col;
            B->data[q].col=A.data[p].row;
            B->data[q].e=A.data[p].e;
            position[col]++;                                                                                                                       
        }
    }
}
