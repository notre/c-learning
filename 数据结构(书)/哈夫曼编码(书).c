//类型定义
#define N 20
#define M 2*N-1
typedef struct 
{
    int weight;     //结点的权值
    int parent;     //双亲的下标
    int LChild;     //左孩子结点的下标
    int RChild;     //右孩子结点的下标
}HTNode,HuffmanTree[M+1];   //HuffmanTree是一个结构数组类型，0号单元不用

//定义 HuffmanTree ht;
//创建哈夫曼树算法
void CrtHuffmanTree(HuffmanTree ht,int w[],int n)
{
    for(i=1;i<=n;i++)   ht[i]={w[i],0,0,0};     //w[i]是权值
    m=2*n-1;
    for(i=n+1;i<=m;i++) ht[i]={0,0,0,0};

    for(i=n+1;i<=m;i++){
        select(ht,i-1,&s1,&s2);                 //在ht[1]~ht[i-1]的范围内选择两个parent为0且weight最小的结点，
        ht[i].weight=ht[s1].weight+ht[s2].weight;                   //其序号分别赋值给s1，s2
        ht[s1].parent=i;ht[s2].parent=i;
        ht[i].LChild=s1;ht[i].RChild=s2;
    }
}

//select部分
void select(int *ht,int a,int *s1,int *s2)
{
    int p;
    *s1=ht[1].weight;
    for(int i=2;i<=a;i++){
        if((ht[i].weight<*s1)&&ht[i].parent==0)
            *s1=i;p=i;
    }
    *s2=ht[1].weight;
    for(int j=1;j<=a;j++){
        if((ht[j].weight<*s2)&&ht[j].parent==0&&ht[j].weight>ht[p].weight)
        *s2=ht[j].weight;
    }
}

//求哈夫曼树的哈夫曼编码的算法
typedef char *HUffmanCode[N+1];         //存储哈夫曼编码串的头指针数组

void CrtHuffmanCode(HuffmanTree ht,HuffmanCode hc,int n)    //从叶子结点到根，逆向求每个叶子结点对应的哈弗曼编码
{
    char *cd;
    cd=(char*)malloc(n*sizeof(char));           //分配求当前编码工作空间
    cd[n-1]='\0';                               //从右向左逐位存放编码，首先存放编码结束符
    
    for(i=1;i<=n;i++)                           //求n个叶子结点对应的哈夫曼编码
    {
        start=n-1;                              //初始化编码起始指针
        c=i;p=ht[i].parent;                     //从叶子结点开始向上倒推
        while(p!=0){
            --start;
            if(ht[p].LChild==c) cd[start]='0';  //左分支标0
            else    cd[start]='1';              //右分支标1
            c=p;p=ht[p].parent;                 //向上倒推
        }
        hc[i]=(char*)malloc((n-start)*sizeof(char));    //为第i个编码分配空间
        strcpy(hc[i],&cd[start]);                       //把编码复制到hc[i]中
    }
    free(cd);
}