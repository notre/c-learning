//深度优先找出从顶点u到v的简单路径
int *pre;
void one_path(Graph *G,int u,int v)
{
    int i;
    pre=(int*)malloc(G->vexnum*sizeof(int));
    for(i=0;i<G->vexnum;i++)    pre[i]=-1;
    pre[u]=-2;                              //将pre[u]置为-2，表示初始顶点u已被访问，并且u没有前驱
    DFS_path(G,u,v);                       
    free(pre);
}

int DFS_path(Graph *G,int u,int v)
{
    int j;
    for(j=firstadj(G,u);j>=0;j=nextadj(G,u,j))
        if(pre[j]==-1){
            pre[j]=u;
            if(j==v){
                print_path(pre,v);              //从v开始，沿着pre[]中保留的前驱指针输出路径，直到-2
                return 1;
            }
            else if(DFS_path(G,j,v))    return 1;
        }
    return 0;
}

//普里姆算法（求最小生成树）
struct{
    int adjvex;
    int lowcost;
}closedge[MAX_VERTEX_NUM];      //求最小生成树时的辅助数组

MiniSpanTree_Prim(AdjMatrix gn,int u)   //从顶点u出发，构造连通网gn的最小生成树，并输出生成树的每条边
{
    closedge[u].lowcost=0;      //初始化，U={u}
    for(i=0;i<gn.vexnum;i++)
        if(i!=u){               //对V-U中的顶点i，初始化closedge[i]
            closedge[i].adjvex=u;
            closedge[i].lowcost=gn.arcs[u][i].adj;
        }
    for(e=1;e<=gn.vexnum-1;e++){         //找n-1条边(n=gn.vexnum)
        v=Minium(closedge);              //closedge[v]中存有当前最小边(u,v)的信息
        u=closedge[v].adjvex;            //u∈U
        printf(u,v);                //输出生成树的当前最小边(u,v)
        closedge[v].lowcost=0;      //将顶点v纳入U集合中
        for(i=0;i<vexnum;i++)       //在顶点v并入U后，更新closedge[i]
            if(gn.arcs[v][i].adj<closedge[i].lowcost){
                closedge[i].lowcost=gn.arcs[v][i].adj;
                closedge[i].adjvex=v;
            }
    }
}

//狄杰斯特拉算法（图的最短路径算法）
#define INFINITY 32768
typedef unsigned WeightType
typedef WeightType AdjType
typedef SeqList VertexSet

ShortesPath_DJS(AdjMatrix g,int v0,WeightType dist[MAX_VERTEX_NUM],VertexSet path[MAX_VERTEX_NUM])
{
    VertexSet d;
    for(i=0;i<g.arcs[v0][i];i++){
        if(dist[i]<INFINITY){
            AddTail(&path[i],g.vertex[v0]);
            AddTail(&path[i],g.vertex[i]);
        }
    }
    InitList(&s);
    AddTail(&s,g.vertex[v0]);
    for(t=1;t<=g.vernum-1;t++){
        min=INFINITY;
        for(i=0;i<g.vernum;i++)
            if(!Member(g.vertex[i],s)&&dist[i]<min)
                {k=i;min=dist[i];}
        if(min==INFINITY)   return;
        AddTail(&s,g.vertex[k]);
        for(i=0;i<g.vernum;i++)
            if(!Member(g.vertex[i],s)&&g.arcs[k][i].adj!=INFINITY&&(dist[k]+g.arcs[k][i].adj<dist[i])){
                dist[i]=dist[k]+g.arcs[k][i].adj;
                path[i]=path[k];
                AddTail(&path[i],g.vertex[i]);
            }
    }
}