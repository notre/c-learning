//头尾链表存储结构类型定义
typedef enum {ATOM,LIST} ElemTag;   //ATOM=0,表示原子；LIST=1,表示子表
typedef struct GLNode
{
    ElemTag tag;                    //标志位tag用来区别原子结点和表结点
    union 
    {
        AtomType atom;              //原子结点的值域atom
        struct {struct GLNode *hp,*tp;}htp; // 表结点的指针域htp，表头指针域hp，表尾指针域tp
    }atom_htp;          //atom_htp是原子节点的值域atom和表结点的指针域htp的联合体域
}GLNode,*GList;

//同层结点链存储结构类型定义
/*typedef enum {ATOM,LIST} ElemTag;
typedef struct GLNode
{
    ElemTag tag;
    union 
    {
        AtomType atom;
        struct GLNode *hp;          //表头指针域
    }atom_hp;
    struct GLNode *tp;              //同层下一节点的指针域
}GLNode,*GList;*/

//求广义表L的表头，并返回表头指针
GList Head(GList L)
{
    if(L==NULL) return(NULL);       //空表无表头
    if(L->tag==ATOM)    exit(0);    //原子不是表
    else    return(L->atom_hp.htp.tp)
}

//求L的表尾，并返回表尾指针
GList Tail(GList L)
{
    if(L==NULL) return(NULL);
    if(L->tag==ATOM)    exit(0);
    else    return(L->atom_htp.htp.hp);
}

//求广义表的长度
int Length(GList L)
{
    int k=0;
    GLNode *s;
    if(L==NULL) return(0);
    if(L->tag==ATOM)    exit(0);
    s=L;
    while(s!=NULL)
    {
        k++;
        s=s->atom_htp.htp.tp;
    }
    return(k);
}

//求广义表的深度
int Length(GList L)
{
    int d,max;
    GLNode *s;
    if(L==NULL) return(0);
    if(L->tag==ATOM)    exit(0);
    s=L;
    while(s!=NULL)           //求每个子表的深度的最大值
    {
        d=Depth(s->atom_htp.htp.hp);
        if(d>max)   max=d;
        s=s->atom_htp.htp.tp;
    }
    return(max+1);
}

//统计广义表中原子数目
int CpuntAtom(GList L)
{
    int n1,n2;
    if(L==NULL) return(0);
    if(L->tag==ATOM)    return(1);
    n1=CpuntAtom(L->atom_htp.htp.hp);   //求表头中的原子数目
    n2=CpuntAtom(L->atom_htp.htp.tp);   //求表尾中的原子数目
    return(n1+n2);
}

//复制广义表
int CopyGList(GList S,GList *T)
{
    if(L==NULL) {*T=NULL;return(OK);}       //复制空表
    *T=(GLNode*)malloc(sizeof(GLNode));
    if(*T==NULL)    return(ERROR);
    (*T)->tag=S->tag;
    if(S->tag==ATOM)    (*T)->atom=S->atom; //复制单个原子
    else{
        CopyGList(S->atom_htp.htp.hp,&((*T)->atom_htp.htp.hp));     //复制表头
        CopyGList(S->atom_htp.htp.tp,&((*T)->atom_htp.htp.tp));     //复制表尾
    }
    return(OK);
}