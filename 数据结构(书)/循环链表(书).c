//初始化双向链表
InitCLinkList(LinkList *CL)
/* CL用来接收待初始化的循环单链表的头指针地址 */
{
    *CL=(LinkList)malloc(sizeof(Node));
    (*CL)->next=*CL;
}

//建立循环单链表
void CreatCLinkList(LinkList CL)
/* 尾插法,CL是已经初始化好的，带头节点的空循环链表的头指针 */
{
    Node *rear,*s;
    char c;
    rear=CL;
    c=getchar();
    while(c!='$')
    {
        s=(Node *)malloc(sizeof(Node));
        s->data=c;
        rear->next=s;
        rear=s;
        c=getchar();
    }
    rear->next=CL;
}

//循环单链表的合并算法(1)
LinkList merge_1(LinkList LA,LinkList LB)
{   /* 将两个采用头指针的循环单链表的首位连接起来 */
    Node *p,*q;
    p=LA;
    q=LB;
    while(p->next!=LA)  p->next;    //使p指向LA的表尾
    while(q->next!=LB)  q->next;    //使q指向LB的表尾
    q->next=LA;
    p->next=LB-next;
    free(LB);
    return(LA);
}   /* 执行时间O(n) */

//循环单链表的合并算法(2)
LinkList merge_2(LinkList RA,LinkList RB)
{   /* 将两个采用尾指针的循环单链表的首位连接起来 */
    Node *p;
    p=RA->next;
    RA->next=RB->next->next;    //链表RB的开始节点链到链表RA的终端节点之后
    free(RB->next);             //释放RB的头结点
    RB->next=p;                 //链表RA的头结点链到链表RB的终端节点之后
    return RB;                  //返回新链表的尾指针
}