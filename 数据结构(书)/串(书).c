（定长顺序串）
//定义
#define MAXLEN 40
typedef struct
{
    char ch[MAXLEN];
    int len;
}SString;

//顺序串插入函数
StrInsert(SString *s,int pos,SString i)
{
    int i;
    if(pos<0||pos>s->len)   return(0);
    if(s->len+t.len<=MAXLEN)
    {
        for(i=s->len+t.len-1;i>=t.len+pos;i--)
            s->ch[i]=s->ch[i-t.len];
        for(i=0;i<t.len;i++)    s->ch[i+pos]=t.ch[i];
            s->len=s->len+t.len;
    }
    else if(pos+t.len<=MAXLEN)
    {
        for(i=MAXLEN-1;i>t.len+pos-1;i--)   s->ch[i]=s->ch[i-t.len];
        for(i=0;i<t.len;i++)    s->ch[i+pos]=t.ch[i];
        s->len=MAXLEN;
    }
    else
    {
        for(i=0;i<MAXLEN-pos;i++)   s->ch[i+pos]=t.ch[i];
        s->len=MAXLEN;
    }
    return(1);
}

//顺序串删除函数
StrDelete(SString *s,int pos,int len)
{
    int i;
    if(pos<0||pos>(s->len-len)) return 0;
    for(i=pos+len;i<s->len;i++)
        s->ch[i-len]=s->ch[i];
    s->len=s->len-len;
    return(1);
}

//串比较函数
StrCompare(SString s,SString t)
{
    int i;
    for(i=0;i<s.len&&i<t.len;i++)
        if(s.ch[i]!=t.ch[i])    return(s.ch[i]-t.ch[i]);
    return(s.len-t.len);
}

//顺序串的简单模式匹配（定位）函数
StrIndex(SString s,int pos,SString t)
{
    int i,j,start;
    if(t.len==0)    return 0;
    start=pos;i=start;j=0;
    while(i<s.len&&j<t.len)
        if(s.ch[i]==t.ch[j])    {i++;j++;}
        else {
            start++;
            i=start;j=0;
        }
    if(j>=t.len)    return(start);
    else    return(-1);
}

(堆串)
//定义
typedef struct 
{
    char *ch;   //ch域指串的起始地址
    int len;
}HString;

//堆串插入函数
StrInsert(HString *s,int pos,HString *t)    //在串s中下标为pos的字符之前插入串t
{
    int i;
    char *temp;
    if(pos<0||pos>s->len||s->len==0)    return(0);
    temp=(char*)malloc(s->len+t->len);
    if(temp==NULL)  return(0);
    for(i=0;i<pos;i++)  temp[i]=s->ch[i];
    for(i=0;i<t->len;i++)   temp[i+pos]=t->ch[i];
    for(i=pos;i<s-len;i++)  temp[i+t->len]=s->ch[i];
    s->len+=t->len;
    free(s->ch);
    s->ch=temp;
    return(1);
}

//堆串赋值函数
StrAssign(HString *s,char *tval)    //将字符串常量的值tval的值赋给堆串s
{
    int len,i=0;
    if(s->ch!=NULL) free(s->ch);
    while(tval[i]!='\0')    i++;
    len=i;
    if(len)
    {
        s->ch=(char*)malloc(len);
        if(s->ch==NULL) return(0);
        for(i=0;i<len;i++)  s->ch[i]=tval[i];
    }
    else s->ch=NULL;
    s->len=len;
    return(1);
}

(块链串)
//定义
#define BLOCK_SIZE 4    //每个结点存放字符数为4
typedef struct Block
{
    char ch[BLOCK_SIZE];
    struct Block *next;
}Block;

typedef struct
{
    Block *head;
    Block *tail;
    int len;
}BLString;

//结点大小等于一
typedef struct chBlock
{
    char ch;
    struct chBlock *next;
}chBlock;