//结构定义
typedef struct DNode
{
    ElemType data;
    struct DNode *prior,*next;
}DNode,* DoubleList;

//双向链表的插入操作
int DlinkIns(DoubleList L,int i,Elemtype e)
{
    DNode *s,*p;
    ···                 //检查插入位置的合法性
    ···                 //若位置i合法，则找到第i个节点并让指针p指向它
    s=(DNode *)malloc(sizeof(DNode));
    if(s)
    {
        s->data=e;
        s->prior=p->prior;
        p->prior->next=s;
        s->next=p;
        p->prior=s;  
        return TRUE;
    }
    else return FALSE;
}

//双向链表的删除操作
int DlinkDel(DoubleList L,int i,Elemtype *e)
{
    DNode *p;
    ···
    ···
    *e=p->data;
    p->prior->next=p->next;
    p->next->prior=p->prior;
    free(p);
    return TRUE;
}