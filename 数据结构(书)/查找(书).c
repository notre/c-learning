(基于线性表的查找法)
//结构定义
#define LIST_SIZE 20
typedef struct{
    KeyType key;
    OtherType other_data;
}RecordType;
typedef struct{
    RecordType r[LIST_SIZE+1];
    int length;
}RecordLsit;

//设置监视哨的顺序查找法
int SeqSearch(RecordList l,KeyType k)
{
    l.r[0].key=k;i=l.length;
    while(l.r[i].key!=k)    i--;    //可以防止越界
    return i;
}       //ASL=(n+1)/2

//不设监视哨的顺序查找法
int SeqSearch(RecordList l,KeyType k)
{
    i=l.length;
    while(i>=1&&l.r[i].key!=k)  i--;
    if(i>=1)    return(i);
    return(0);
}

//折半查找法
int BinSrch(RecordList l;KeyType k)
{
    low=1;high=l.length;
    while(low<=high){
        mid=(low+high)/2;
        if(k==l.r[mid].key) return(mid);
        else if(k<l.r[mid].key) high=mid-1;
        else low=mid+1;
    }
    return 0;
}       //ASL=log(n+1)-1 <<以2为低


(基于树的查找法)
//二叉排序树的的定义
typedef struct node
{
    KeyType key;
    struct node *lchild,*rchild;
}BSTNode,*BSTree;

//二叉排序树的插入的递归算法
void InsertBST(BSTree *bst,KeyType key)
{
    BiTree s;
    if(*bst==NULL){
        s=(BSTree)malloc(sizeof(BSTNode));
        s->key=key;
        s->lchild=NULL;s->rchild=NULL;
        *bst=s;
    }
    else if(key<(*bst)->key)
        InsertBST(&((*bst)->lchild),key);
    else if(key>(*bst)->key)
        InsertBST(&((*bst)->rchild),key);
}

//创建二叉排序树
void CreateBST(BSTree *bst)
{
    KeyType key;
    *bst=NULL;
    scanf("%d",&key);
    while(key!=ENDKEY){
        InsertBST(bst,key);
        scanf("%d",&key);
    }
}

//二叉排序树查找的递归算法
BSTree SearchBST(BSTree bst,keyType key)
{
    if(!bst)    return NULL;
    else if(bst->key==key)  return bst;
    else if(bst->key>key)   return SearchBST(bst->lchild,key);
    else    
        return SearchBST(bst->rchild,key);
}

//二叉排序树查找的非递归算法
BSTree SearchBST(BSTree bst,KeyType key)
{
    BSTree q;
    q=bst;
    while(q){
        if(q->key==key) return q;
        if(q->key>kye)  q=q->lchild;
        else q=q->rchild;
    }
    return NULL;
}

//二叉排序树中删除结点
BFSNode *DelBFS(BSTree t,KeyType k)
{
    BSTNode *p,*f,*s,*q;
    p=t;f=NULL;
    while(p){
        if(p->key==k)   break;
        f=p;
        if(p->key>k)    p=p->lchild;
        else p=p->rchild;
    }
    if(p==NULL) return t;
    if(p->lchild==NULL){
        if(f==NULL) t=p->rchild;
        else if(f->lchild==p)
            f->lchild=p->rchild;
        else    
            f->rchild=p->rchild;
        free(p);
    }
    else{
        q=p;s=p->lchild;
        while (s->rchild){
            q=s;s=s->rchild;
        }
        if(q==p)    q->lchild=s->lchild;
        else q->rchild=s->lchild;
        p->key=s->key;
        free(s);
    }//用的方法二：s替换p，再将s的左子树链接到s的双亲结点的右子树上
    return t;
}

（平衡二叉排序树）
/*         平衡因子   移动
        A(bf)  B(bf)
    LL   2       1    B
    RR  -2      -1    B
    LR   2      -1    C
    RL  -2       1    C
*/

//平衡二叉排序树的插入
void ins_AVLtree(AVLTree *avlt,KeyType k)   //在平衡二叉树上插入元素k
{
    S=(AVLTree)malloc(sizeof(AVLTNode));
    S->key=k;S->lchild=S->rchild=NULL;
    S->bf=0;
    if(*avlt==NULL) *avlt=S;
    else{
            //查找S的插入位置fp，记录距S的插入位置最近且平衡因子不等于0的结点A
        A=*avlt;FA=NULL;
        p=*avlt;fp=NULL;
        while(p!=NULL){
            if(p->bf!=0)    {A=p;FA=fp;}
            fp=p;
            if(K<p->key)    p=p->lchild;
            else p=p->rchild;
        }
    }

    if(K<fp->key)   fp->lchild=S;   //插入S
    else fp->rchild=S;

    if(K<A->key)    {B=A->lchild;A->bf=A->bf+1;}    //确定结点B，并修改A的平衡因子
    else{B=A->rchild;A->bf=A->bf-1;}

    p=B;                //修改B到S路径上各结点的平衡因子（原值均为0）
    while(p!=S){
        if(K<p->key)    {p->bf=1;p=p>lchild}
        else{p->bf=-1;p=p->rchild;}
    }

    //判断相应类型并做相应处理

    if(A->bf==2&&B->bf==1)      //LL型
    {
        A->lchild=B->rchild;
        B->rchild=A;
        A->bf=0;B->bf=0;
        if(FA==NULL)    *avlt=B;
        else if(A==FA->lchild)  FA->lchild=B;
        else FA->rchild=B;
    }

    else if(A->bf==2&&B->bf==-1)    //LR型
    {
        C=B->rhcild;
        B->rchild=C->lchild;
        A->lchild=C->rchild;
        C->lchild=B;C->rchild=A;
        if(S->key<C->key)
            {A->bf=-1;B->bf=0;C->bf=0;}
        else if(S->key>C->key)
            {A->bf=0;B->bf=1;C->bf=0;}
        else
            {A->bf=0;B->bf=1;C->bf=0;}
        if(FA=NULL) *avlt=C;
        else if(A==FA->lchild)  FA->lchild=C;
        else FA->rchild=C;
    }

    else if(A->bf==-2&&B->bf==1)    //RL型
    {
        C=B->lchild;
        B->lchild=C->rchild;
        A->rchild=C->lchild;
        C->lchild=A;C->rchild=B;
        if(S->key<C->key)
            {A->bf=0;B->bf=-1;C->bf=0;}
        else if(S->key>C->key)
            {A->bf=1;B->bf=0;C->bf=0;}
        else
            {A->bf=0;B->bf=0;}
        if(FA==NULL)    *avlt=C;
        else if(A==FA->lchild)  FA->lchild=C;
        else    FA->rchild=C;
    }

    else if(A->bf==-2&&B->bf==-1)   //RR型
    {
        A->rchild=B->lchild;
        B->lchild=A;
        A->bf=0;B->bf=0;
        if(FA==NULL)    *avlt=B
        else if(A==FA->lchild)  FA->lchild=B;
        else FA->rchild=B;
    }
}

//哈希表的查找算法（线性探测法处理冲突）
#define m   <哈希表长度>
#define NULLKEY <代表空记录的关键字值>
typedef struct
{
    KeyType key;
    ...
    ...
}RecordType;
typedef RecordType HashTable[m];

int HashSearch(HasTable ht,KeyType K)
{
    h0=hash(K);
    if(ht[h0].key==NULLKEY) return(-1);
    else if(ht[h0].key==K)  return(h0);
    else{       //用线性探测再散列解决冲突
        for(i=1;i<=m-1;i++){
            hi=(h0+i)%m;
            if(ht[hi].key==NULLKEY) return(-1);
            else if(ht[hi].key==K)  return(hi);
        }
        return(-1);
    }
}