//邻接矩阵表示法
#define MAX_VERTEX_NUM 20       //最多顶点个数
#define INFINITY 32786          //表示极大值，即∞

typedef enum{DG,DN,UDG,UDN}GraphKind;  //图的种类，DG表示有向图，DN表示有向网，UDG表示无向图，UDN表示无向网

typedef char VertexData;        //假设顶点数据为字符型

typedef struct ArcNode
{   
    AdjType adj;        //对于无权图，用0或1表示是否相邻；对有权图，则为权值类型
    OtherInfo info;
}ArcNode;

typedef struct 
{
    VertexData vertex[MAX_VERTEX_NUM];              //顶点向量
    ArcNode arcs[MAX_VERTEX_NUM][MAX_VERTEX_NUM];   //邻接矩阵
    int vexnum,arcnum;            //图的顶点数和弧度
    GraphKind Kind;               //图的种类标志
}AdjMatrix;

//采用邻接矩阵表示法创建有向图
int LocateVertex(AdjMatrix *G,VertexData v)         //求顶点位置函数
{
    int j=Error,k;
    for(k=0;k<G->vexnum;k++)
        if(G->vertex[k]==v)
            {j=k;break;}
    return(j);
}

int CreateDN(AdjMatrix *G)              //创建一个有向网
{
    int i,j,k,weight;VertexData v1,v2;
    scanf("%d,%d",&G->arcnum,&G->vexnum);   //输入图的顶点数和弧度，arcnum是弧度，vexnum是顶点数

    for(i=0;i<G->vertex;i++)                //初始化邻接矩阵
        for(j=0;j<G->vertex;j++)
            G->arcs[i][j].adj=INFINITY;

    for(i=0;i<G->vexnum;i++)
        scanf("%c",&G->vertex);                 //输入图的顶点

    for(k=0;k<G->arcnum;k++){
        scanf("%c,%c,%d",&v1,&v2,&weight);  //输入一条弧的两个顶点及权值
        i=LocateVex_M(G,v1);
        j=LocateVex_M(G,v2);
        G->arcs[i][j].adj=weight;           //建立弧
    }
    return(OK);
}

//邻接表表示法结构定义
#define MAX_VERTEX_NUM 20                   //最多顶点数
typedef enum{DG,DN,UDG,UDN} GraphKind;
typedef struct ArcNode{
    int adjvex;                             //该弧指向顶点的位置
    struct ArcNode *nextarc;                //指向下一条弧的指针
    OtherInfo info;                         //与该弧相关的信息
}ArcNode;

typedef struct VertexNode{
    VertexData data;                        //顶点数据
    ArcNode *firstarc;                      //指向该顶点第一条弧的指针
};

typedef struct{
    VertexNode vertex[MAX_VERTEX_NUM];
    int vernum,arcnum;                      //图的顶点数和弧数
    GraphKind;
}AdjList;

//十字链表结构定义
#define MAX_VERTEX_NUM 20;
typedef enum{DG,DN,UDG,UDN} GraphKind;

typedef struct ArcNode{
    int tailver,headvex;
    struct ArcNode *hlink,*tlink;
}ArcNode;

typedef struct VertexNode{
    VertexData data;                        //顶点信息
    ArcNode *firstin,*firstout;
}VertexNode;

typedef struct{
    VertexNode vertex[MAX_VERTEX_NUM];
    int vertex,arcnum;
    GraphKind kind;
}OrthList;

//创建十字链表
void CrtOrthList(OrthList *g)   //从终端输入n个顶点的信息和e条弧的信息，建立一个有向图的十字链表
{
    scanf("%d,%d",&n,&e);       //从键盘输入图的顶点个数和弧的个数
    g->vernum=n;
    g->arcnum=e;
    for(i=0;i<n;i++){
        scanf("%c",&(g->vertex[i].data));
        g->vertex[i].firstin=NULL;
        g->vertex[i].firsout=NULL;
    }
    for(k=0;k<e;k++){
        scanf("%c,%c",&vt,&vh);
        i=LocateVertex(g,vt);
        j=LocateVertex(g,vh);
        p=(ArcNode*)malloc(sizeof(ArcNode));
        p->tailvex=i;p->headvex=j;
        p->tlink=g->vertex[i].firstout;
        g->vertex[i].firstout=p;
        p->hlink=g->vertex[j].firstin;
        g->vertex[j].firstin=p;
    }
}