//双亲表示法形式说明
#define MAX 100
typedef struct TNode
{
    DataType data;
    int parent;
}TNode;
typedef struct            //树定义
{
    TNode tree[MAX];
    int nodenum;
}ParentTree;

//以树（森林）的双亲表示法为基础，并查集可定义为
typedef ParentTree ParentForest;
typedef ParentForest MFSst;

//初始化并查集
void Initialization(MFSst *SS,SeqList *S)     //用S中的n各元素构造n个单根树，代表n个单元素集合S0,S1,···，Sn-1，
{                                               //这n个单根树构成一个森林，代表并查集SS
    int i;
    SS->nodenum=S->last+1;
    for(i=0;i<SS->nodenum;i++)
    {
        SS->tree[i].data=S->elem[i];
        SS->tree[i].parent=-1;
    }
}

//在并查集中查找某个元素（a）
int Find_l(MFSst *SS,DataType x)
{
    pos=Locate(SS,x);               //确定x在SS->tree[]中的下标
    if(pos<0)   return -1;              
    i=pos;
    while(SS->tree[i].parent>0)     //从pos开始，沿着双亲指针查找根结点
        i=SS->tree[i].parent;
    return i;                       //返回x所在子集树的根结点下标
}

//合并并查集中的子集树（a）
int Merge_l(MFSst *SS,int root1,int root2)          //root1和root2是并查集SS中两个互不相交的非空子集树的根，
{                                                      //将子集树root2并入子集树root1
    if(root1<0||root1>SS->nodenum-1)    return ERROR;
    if(root2<0||root2>SS->nodenum-1)    return ERROR;
    SS->tree[root2].parent=root1;
    return OK;
}

//合并并查集中的子集树（b）
int Merge_2(MFSst *SS,int root1,int root2)  //root1和root2是并查集SS中两个互不相交的非空子集树的根，根结点的parent域
{                                             //存放树中结点数目的负值。此算法将结点数目较少的子集树并入结点数目较多的子集树中              
    if(root1<0||root1>SS->nodenum-1)    return ERROR;
    if(root2<0||root2>SS->nodenum-1)    return ERROR;
    if(SS->tree[root1].parent<SS->tree[root2].parent)   //第一棵子集树中结点数目较多
    {
        SS->tree[root2].parent=root1;
        SS->tree[root1].parent+=SS->tree[root2].parent;
    }
    else{
        SS->tree[root1].parent=root2;
        SS->tree[root2].parent+=SS->tree[root1].parent;
    }
    return OK;
}

//在并查集中查找某个元素（b）
int Find_2(MFSst *SS,DataType x)
{
    pos=Locate(SS,x);
    if(pos<0)   return -1;
    i=pos;
    while(SS->tree[i].parent>0)
        i=SS->tree[i].parent;
    root=i;                     //记录x所在子集树的根节点下标
    i=pos;                      //从pos开始，将x及x的所有祖先（除了root）均改为root的子节点
    while(i!=root){
        temp=SS->tree[i].parent;
        SS->tree[i].parent=root;
        i=temp;
    }
    return root;
}