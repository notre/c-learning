//定义
#define Stack_Size 50
typedef struct 
{
    StackElementType elem[Stack_Size];
    int top;    //top为-1表示空栈
}SeqStack;

//初始化
void InitStack(SeqStack *S)
{
    S->top=-1;
}

//进栈
int Push(SeqStack *S,StackElementType x)
{
    if(S->top==Stack_Size-1)    return(FALSE);
    S->top++;
    S->elem[S->top]=x;
    return(TRUE);
}

//出栈
int Pop(SeqStack *S,StackElementType *x)
{
    if(S->top==-1)
        return(FALSE);
    else
    {
        *x=S->elem[S->top];
        S->top--;
        return(TRUE);
    }
}

//读栈顶元素
int GetTop(SeqStack *S,StackElementType *x)
{
    if(S->top==-1)  return(FALSE);
    else
    {
        *x=S->elem[S->top];
        return(TRUE);
    }
}


//多栈共享技术↓↓↓

//两栈共享数据结构定义
#define M 100
typedef struct 
{
    StackElementType Stack[M];
    StackElementType top[2];
}DqStack;

//双端顺序栈初始化
void InitStack(DqStack)
{
    S->top[0]=-1;
    S->top[1]=M;
}

//双端顺序栈进栈操作
int Push(DqStack *S,StackElementType x,int i)
{
    if(S->top[0]+1==S->top[1])
        return(FALSE);
    switch (i)
    {
    case 0:
        S->top[0]++;
        S->Stack[S->top[0]]=x;
        break;
    case 1:
        S->top[1]--;
        S->Stack[S->top[1]]=x;
        break;
    default:
        return(FALSE);
    }
    return(TRUE);
}

//双端顺序栈出栈操作
int Pop(DqStack *S,StackElementType *x,int i)
{
    switch(i)
    {
        case 0:
            if(S->top[0]+1==S->top[1])  return(FALSE);
            *x=S->Stack[S->top[0]];
            S->top[0]--;
            break;
        case 1:
            if(S->top[0]+1==S->top[1])  return(FALSE);
            *x=S->Stack[S->top[1]];
            S->top[1]++;
            break;  
        default:
            return(FALSE);  
    }
    return(TRUE);
}
