#include<stdio.h>
int main()
{
	void swap(int * p1,int * p2);
	int a,b;
	int * p3,* p4;
	printf("请输入两个整数：");
	scanf("%d,%d",&a,&b);
	p3=&a;
	p4=&b;
	if(a<b)	swap(p3,p4);
	printf("max=%d,min=%d\n",a,b);
	return 0;
}

void swap(int * p1,int * p2)
{
	int t;
	t=* p1;
	* p1=* p2;
	* p2=t;
}

