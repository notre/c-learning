#include<stdio.h>
int main()
{
	float average(float array[],int n);
	int i;
	float score1[5]={23.4,56,78.3,48.6,66.23};
	float score2[10]={51.2,36,77.4,68.6,65.23,53.4,76,28.3,88.6,46.23};
	printf("1班的平均成绩：%6.2f\n",average(score1,5));
	printf("2班的平均成绩：%6.2f\n",average(score2,10));
	return 0;
}

float average(float array[],int n)
{
	int i;
	float aver,sum=array[0];
	for(i=1;i<n;i++)
		sum=sum+array[i];
	aver=sum/n;
	return aver;
}
