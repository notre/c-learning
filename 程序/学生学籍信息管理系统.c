#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<conio.h>
#define N 100 /*存储100个学生的学籍信息*/
int flag; /*标记是否登录*/
struct date /*出生日期*/
{
    int year;
    int month;
    int day;
};
struct student
{
	char num[20];/*学号*/
	char name[25];/*姓名*/
	char sex[10];/*性别*/
	struct date birthday;/*出生年月*/
	int age;/*年龄*/
	char addr[50];/*籍贯*/
	char dept[50]; /*系别*/
    char major[30]; /*专业*/
	char classs[30]; /*班级*/
	char phone[20];/*电话*/
}stu[N];
 
void login() /*登录*/
{
	void menu();
	system("cls");
	if(flag)
	{
		printf("  已经登录!\n");
		printf("Press any key to continue…\n");
		getchar();
		getchar();
		system("cls");
		menu();
	}
	int load();
	int n,i,m,w;
	FILE *fp;
    if((fp=fopen("D:\\file.txt","rb"))!=NULL) /*不是空文件*/
      n=load(); /*打开文件*/
	else
	  n=0;
	struct student s;
	char password[20];
	int d,q;
	printf("\n\t请选择登录身份：1.管理员      2.用户     [ ]\b\b");
    scanf("%d",&d);
	if(d==1)
	{
		printf("\n\t\t\t请输入密码：");
		getchar();
		int j=0;
		while(1)
        {
            password[j]=getch();
            if(password[j]=='\r')
              break;
            printf("*");
            j++;
        }
        password[j]='\0';
		if(!strcmp(password,"00000000")) /*判断密码是否正确*/
		{
			flag=1;
			printf("\n\t登录成功!\n");
			printf("\tPress [ Enter ] key to continue…");
			getchar();
			system("cls");
			menu();
		}
		if(!flag)
		{
		   printf("\t\n密码错误!\n");
           printf("What do you want to do next? 1.继续登录  2.返回菜单   [ ]\b\b");
		   scanf("%d",&q);
		   if(q==1) /*继续登录*/
		   {
			   system("cls");
		       login();
		   }
		   else if(q==2) /*返回主菜单*/
		   {
			   system("cls");
			   menu();
		   }
		}
	}
    else
	{
	   printf("\n\t\t\t用户名:");
	   scanf("%s",s.name);
	   printf("\t\t\t密码:");
	   int j=0;
	   while(1)
        {
            password[j]=getch();
            if(password[j]=='\r')
              break;
            printf("*");
            j++;
        }
       password[j]='\0';
       flag=0; /*未登录时flag=0*/
	   for(i=0;i<n;i++)
	     if(!strcmp(s.name,stu[i].name)&&!strcmp(s.name,password))
		 {
		   flag=1; /*登录成功flag=1*/
		   break;
		 }
	  if(flag)
	  {
		 printf("\n\t登录成功!\n");
		 printf("\tPress any key to continue…\n");
		 getchar(); getchar();
		 system("cls");
	     menu(); /*登录成功后返回主菜单*/
	  }
	  else
	  {
		m=0; /*设置m是为了避免陷入 登录失败时 跳不出登录界面*/
		w=1; /*设置w是为了避免输出多次 “用户不存在”*/
		int q; /*q为选项*/
		do
		{
			if(w>0)
			   printf("用户名不存在或密码错误!\n"); /*第一次登录时输入不存在的用户时显示*/
		    printf("What do you want to do next? 1.继续登录  2.返回菜单   [ ]\b\b");
		    scanf("%d",&q);
		    if(q==1) /*继续登录*/
			{
				login();
				m=1;
			}
		    else if(q==2) /*返回主菜单*/
			{
				system("cls");
				menu();
				m=1;
			}
			else
			{
				printf("选择错误!请重新选择:\n");
				w=0;
			}
		}while(m==0);
	  }
	}
}
 
void printf_one(int k) /*输出一个学生的信息*/
{
	system("color 4e");
	printf("学号：%s\n",stu[k].num);
	printf("姓名：%s\n",stu[k].name);
	printf("性别：%s\n",stu[k].sex);
	printf("出生年月：%d/%d/%d\n",stu[k].birthday.year,stu[k].birthday.month,stu[k].birthday.day);
	printf("年龄：%d\n",stu[k].age);
    printf("籍贯：%s\n",stu[k].addr);
	printf("系别：%s\n",stu[k].dept);
	printf("专业：%s\n",stu[k].major);
	printf("班级：%s\n",stu[k].classs);
	printf("电话：%s\n",stu[k].phone);
}
 
void printf_n(int n) /*浏览时输出所有学生的信息*/
{
	system("color 2e");
	int j,i=1;
    for(j=0;j<n;j++)
	{
		printf("第%d个学生：\n",i++);
		printf_one(j);
 		printf("\n");
	}
  getchar();
}
 
int load() /*打开文件,读取里面的数据*/
{
	FILE *fp;
    int i;
    if((fp=fopen("D:\\file.txt","rb"))==NULL) /*读入空文件，rb表示打开一个二进制文件，只允许读数据*/
	{
		printf("\n文件不存在!请输入:\n");
        return 0;
	}
    for(i=0;!feof(fp);i++)  /*处理到文件结尾*/
       fread(&stu[i],sizeof(struct student),1,fp);  /*fread(buffer,size,cout,fp)将磁盘文件中的一批数据作为一个整体一次性读取出来,
	buffer指定数据块位置，sizeof数据块字节数，count读取次数，fp指向源磁盘文件*/
    fclose(fp);
    return(i-1);
}
 
void input(int k) /*输入学生信息*/
{
	getchar();
	printf("学号：");   gets(stu[k].num);
	printf("姓名：");   gets(stu[k].name);
	printf("性别：");   gets(stu[k].sex);
	printf("出生年月：");  scanf("%d/%d/%d",&stu[k].birthday.year,&stu[k].birthday.month,&stu[k].birthday.day);
	printf("年龄：");   scanf("%d",&stu[k].age);  getchar();
    printf("籍贯：");   gets(stu[k].addr);
	printf("系别：");   gets(stu[k].dept);
	printf("专业：");   gets(stu[k].major);
	printf("班级：");   gets(stu[k].classs);
	printf("电话：");   gets(stu[k].phone);
}
 
void save(int n) /*保存学生信息*/
{
	void exit();
	FILE *fp;
	int i;
    if((fp=fopen("D:\\file.txt","wb"))==NULL) /*只打开或建立一个二进制文件，只允许写数据*/
	{
		printf("\nCan not open file!\n");
        exit();
	}
    for(i=0;i<n;i++)
      if(stu[i].name[0]!='\0')
         if(fwrite(&stu[i],sizeof(struct student),1,fp)!=1) /*将一批数据作为一个整体一次性写入磁盘文件*/
            printf("文件写入错误!\n");
    fclose(fp);
}
 
void printf_back()/*添加学生信息后显示的“是浏览还是返回”*/
{
   int w,k=0;
   void browse();
   void menu();
   printf("\n\n\t^_^.插入成功. ^_^\n\n");
   do
   {
      printf("What do you want to do next?\n\n\t1).浏览所有\t2).返回:  [ ]\b\b");
      scanf("%d",&w);
      if(w==1)
	  {
		  system("cls");
		  browse();
	  }
      else if(w==2)
	  {
		  system("cls");
		  menu();
	  }
      else
	      printf("  选择错误!请重新选择!\n");
   }while(k==0);
}
 
void insert() /*添加学生信息*/
{
    int i,n,k,t;
    FILE *fp;
    if((fp=fopen("D:\\file.txt","rb"))==NULL) /*读入空文件，rb表示打开一个二进制文件，只允许读数据*/
	{
		printf("How many people do you want to add(1-%d)?:",N-1);
		n=0;
	}
	else
	{
	  n=load();
      printf("How many student do you want to insert (1-%d)?:",N-n-1);
	}
    scanf("%d",&t);
    k=n+t;
    for(i=n;i<k;i++)
	{
	   printf("\n输入第 %d 个学生学籍信息.\n",i-n+1);
	   input(i);
	}
    save(k); /*保存所有学生的信息*/
    printf_back();
}
 
void deleter() /*删除*/
{
	void menu();
	if(!flag)
	{
		printf("\n\n\t请先登录!\n");
		printf("\n\t请按 Enter 键进入登录界面!");
		getchar();
		getchar();
		login();
	}
	else
	{
	    struct student s;
	    int i,n,k,w0,w1;
        n=load();
        do
		{
	        k=-1;
            //printf_n(n); /*删除之前先显示所有学生的信息*/
            do
			{
	             printf("\n\n请输入您想要删除的学生的姓名!\nName:");
                 scanf("%s",s.name);
                 for(i=0;i<n;i++)
                   if(strcmp(s.name,stu[i].name)==0)
				   {
		              k=i;
		              s=stu[i];
				   }
                 if(k==-1)
				 {
					 int m;
					 printf("\n\n没有这个学生的信息!\n");
                     printf("What do you want to do next? 1.继续输入  2.返回主菜单    [ ]\b\b");
                     scanf("%d",&m);
					 if(m==1)
						 deleter();
					 else
					 {
						 k=0;
						 menu();
					 }
				 }
			}while(k==-1);
	        printf_one(k);
            printf("\n\t确定删除?\n\t1).是   2).返回  [ ]\b\b");/*再次询问是否删除*/
            scanf("%d",&w0);
            if(w0==1)
			{
		       stu[k].name[0]='\0';
		       save(n);
			}
	        else
		        break;
            printf("\n\t^_^.成功删除^_^.\n");
            printf("What do you want to do?\n\t1).继续   2).返回  [ ]\b\b");
            scanf("%d",&w1);
		}while(w1==1);
	  system("cls");
      menu(); /*不再删除时返回主菜单，w1!=1*/
	}
}
 
void search() /*查找*/
{
	void menu();
	int w0,k,i,n,w1,w2;
	struct student s;
	FILE *fp;
    if((fp=fopen("D:\\file.txt","rb"))!=NULL) /*不是空文件*/
	   n=load();
    do
	{
		printf("请选择查找类别:  1.姓名  2.学号   [ ]\b\b");
        scanf("%d",&w1);
		if(w1<1||w1>2)
		{
			printf("输入错误!请重新输入!\n");
			w2=1;
		}
		else
			w2=0; /*这里要注意：不加这句话会陷入死循环，跳不出来*/
	}while(w2==1);
    if(w1==1) /*按姓名查找*/
	{
		do
		{
			k=-1;
	        do
			{
				printf("\n请输入您想要查找的学生的姓名!\nName:");
                scanf("%s",s.name);
                for(i=0;i<n;i++)
                  if(strcmp(s.name,stu[i].name)==0)
				  {
					  k=i;
					  s=stu[i];
				  }
                  if(k==-1) /*没有找到*/
				  {
				     int o;
			         printf("\n\n没有这个学生的信息!\n!");
				     printf("What do you want to do?\n\t1.继续    2.返回菜单  [ ]\b\b");
				     scanf("%d",&o);
				     if(o==1)
					 {
						 system("cls");
						 search(); /*继续查找*/
					 }
				     else
					 {
						 system("cls");
						 menu();
					 }
				  }
			}while(k==-1);
		   system("cls");
           printf_one(k); /*找到以后输出这个学生的信息*/
           printf("\nWhat do you want to do next?\n\t1).继续    2).返回菜单   [ ]\b\b");
           scanf("%d",&w0);
		}while(w0==1);
	  system("cls");
      menu(); /*不再查找时返回主菜单*/
	}
    else /*按学号查找*/
	{
        do
		{
	       k=-1;
	       do
		   {
	           printf("\n请输入您想要查找的学生的学号!\nNum:");
               scanf("%s",s.num);
               for(i=0;i<n;i++)
                 if(strcmp(s.num,stu[i].num)==0) /*找到了*/
				 {
			       k=i;
			       s=stu[i];
				 }
               if(k==-1) /*没有找到*/
			   {
				  int o;
			      printf("\n\n没有这个学生的信息!\n!");
				  printf("What do you want to do?\n\t1.继续    2.返回菜单  [ ]\b\b");
				  scanf("%d",&o);
				  if(o==1)
				  {
					  system("cls");
					  search(); /*继续查找*/
				  }
				  else
				  {
					  system("cls");
					  menu();
				  }
			   }
		   }while(k==-1);
		   system("cls");
	       printf_one(k); /*找到以后输出这个学生的学籍信息*/
           printf("\nWhat do you want to do?\n\t1).继续   2).返回菜单   [ ]\b\b");
           scanf("%d",&w0);
		}while(w0==1);
	  system("cls");
      menu();/*不再查找时返回主菜单*/
	}
}
 
int modify_data(int i) /*修改信息函数*/
{
	int c,w1;
	void menu();
    do
	{
		puts("\n请选择要修改的选项：\n\n1.学号 2.姓名  3.性别  4.出生日期  5.年龄 6.籍贯 7.系别 8.专业 9.班级 10.电话   11.所有信息 12.取消并返回");
        printf("请选择?: [  ]\b\b\b");
        scanf("%d",&c);
        if(c>12||c<1)
		{
			puts("\n选择错误!请重新选择!");
			getchar();
		}
	}while(c>12||c<1);
   do
   {
	   switch(c)
	   {
	     case 1: printf("学号:");  scanf("%s",stu[i].num); break;
         case 2: printf("姓名:");  scanf("%s",stu[i].name); break;
         case 3: printf("性别:");  scanf("%s",stu[i].sex); break;
         case 4: printf("出生年月:");  scanf("%d/%d/%d",&stu[i].birthday.year,&stu[i].birthday.month,&stu[i].birthday.day); break;
		 case 5: printf("年龄:");  scanf("%d",&stu[i].age);break;
		 case 6: printf("籍贯:");  scanf("%s",stu[i].addr);break;
		 case 7: printf("系别:");  scanf("%s",stu[i].dept);break;
		 case 8: printf("专业:");  scanf("%s",stu[i].major);break;
		 case 9: printf("班级:");  scanf("%s",stu[i].classs);break;
		 case 10: printf("电话:");  scanf("%s",stu[i].phone);break;
		 case 11: input(i); break;  /*修改这个学生的所有信息*/
		 case 12: menu();  /*返回主菜单*/
	   }
       puts("\nNow:\n");
       printf_one(i); /*输出 修改后 的这个学生的信息*/
       printf("\n确定?\n\n\t1).是  2).不，重新修改  3).返回不保存 [ ]\b\b");
       scanf("%d",&w1);
   }while(w1==2);
   return(w1); /*修改成功并选择1后返回1*/
}
 
void modify() /*修改*/
{
	void menu();
	if(!flag)
	{
		printf("\n\n\t请先登录!\n");
		printf("\n\t请按 Enter 键进入登录界面!");
		getchar();
		getchar();
		login();
	}
	else
	{
	   struct student s;
	   int i,n,k,w0=1,w1,w2=0;
       n=load();
       do
	   {
	      k=-1;
          //printf_n(n); /*删除之前先显示所有学生的信息*/
          do
		  {
	         printf("\n请输入您想要修改的学生的姓名!\nName:");
             scanf("%s",s.name);
             for(i=0;i<n;i++)
               if(strcmp(s.name,stu[i].name)==0) /*与其中一个学生匹配*/
			   {
		           k=i;
		           s=stu[i];
			   }
             if(k==-1) /*没有找到输入的学生*/
			 {
				 int o;
			     printf("\n\n没有这个学生的信息!\n");
				 printf("What do you want to do?\n\t1.继续    2.返回菜单  [ ]\b\b");
				 scanf("%d",&o);
				 if(o==1)
				 {
					 system("cls");
					 modify(); /*继续修改*/
				 }
				 else
				 {
					system("cls");
					menu();
				 }
			 }
		  }while(k==-1);
		  system("cls");
	      printf_one(k); /*输出这个学生的信息*/
          w1=modify_data(k);
          if(w1==1) /*判断是否修改成功修改*/
		  {
		     printf("\n\t^_^.修改成功 ^_^.\n\n是否继续?\n\n\t1).是 2).保存返回\t[ ]\b\b");
             scanf("%d",&w0);
			 w2=1;
		  }
          else
		  {
		     w0=0;
		     if(w2==0)
			     stu[k]=s;
		  }
          if((w0!=1)&&(w2==1)) /*修改成功保存*/
	         save(n);
	   }while(w0==1);
	   system("cls");
     menu();/*不再修改返回主菜单*/
	}
}
 
void browse()/*浏览*/
{
	system("color 5f");
    void menu();
    int n;
    n=load();
    printf_n(n);
    printf("  共有 %d 个学生的记录.\n",n);
    printf("\nPress [ ENTER ] key to back...");
    getchar();
	system("cls");
    menu(); /*返回主函数*/
}
 
void exit() /*退出*/
{
	exit(0);
}
 
void face()  /*访问页面*/
{
	system("color 4e");
	printf("\n\t    ★ ★ ★ ★ ★ ★ ★ ★ ★ ★ ★ ★ ★ ★ ★ ★ ★ ★\n\n");
	puts("\t    ※※※※※※※※※※※※※※※※※※※※※※※※※※※");
	puts("\t    ※※                                              ※※");
	puts("\t    ※※                                              ※※");
	puts("\t    ※※           欢迎访问学生学籍管理系统!          ※※");
	puts("\t    ※※                                              ※※");
	puts("\t    ※※                                              ※※");
	puts("\t    ※※                                              ※※");
	puts("\t    ※※                                              ※※");
	puts("\t    ※※                                              ※※");
	puts("\t    ※※                                              ※※");
    puts("\t    ※※                                              ※※");
	puts("\t    ※※※※※※※※※※※※※※※※※※※※※※※※※※※");
	printf("\n\tPress [ Enter ] key to continue……\n");
	getchar();
	system("cls");
}
 
void menu() /*主菜单*/
{
	system("color 2e");
	printf("\n\t\t\t欢迎使用学生学籍管理系统!\n");
	printf("\n");
	int n,w1;
    do
	{
	   puts("\t\t★★★★★★★★    菜单  ★★★★★★★★");
	   puts("\t\t★               1.登  录               ★");
	   puts("\t\t★               2.插  入               ★");
	   puts("\t\t★               3.删  除               ★");
	   puts("\t\t★               4.查  找               ★");
	   puts("\t\t★               5.修  改               ★");
	   puts("\t\t★               6.浏  览               ★");
	   puts("\t\t★               7.退  出               ★");
	   puts("\t\t★★★★★★★★★★★★★★★★★★★★★");
	   printf("请选择服务种类(1-7) : [ ]\b\b");
	   scanf("%d",&n);
	   if(n<1||n>7)
	   {
		   system("cls");
		   printf("选择错误!  请重新选择!\n");
		   w1=1;
	   }
	   else
		   w1=0;
	}while(w1==1);
	switch(n)
	{
	   case 1: login();  break; /*登录*/
	   case 2: system("cls");  insert();   break; /*插入*/
	   case 3: system("cls");  deleter();  break; /*删除*/
	   case 4: system("cls");  search();  break; /*查找*/
	   case 5: system("cls");  modify();  break; /*修改*/
	   case 6: system("cls");  browse();  break; /*浏览*/
	   case 7: exit(); break; /*退出*/
	}
}
 
int main()
{
	face();
    menu();
    return 0;
}