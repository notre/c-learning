#include <stdio.h> 
#include <stdlib.h>  
#include <string.h>  

typedef struct curr 
{ 
    char name[20];        //课程姓名  
    int number;           //课程序号          
    char kind[20];        //课程类型 
    int time;             //课程课时  
    int score;            //课程学分  
    struct curr *next;    //链表next  
 }curr,*pcurr; 

typedef struct stu 
{ 
    int number;             //学生序号  
    char name[20];          //学生姓名  
    struct curr *choices;   //学生选课  
    struct stu *next;       //链表next
}stu,*pstu; 

pcurr creat_curr(pcurr curr_head);                          //创建结点，课程信息  
pcurr find_curr(pcurr curr_head,int number);                //查找链表，课程信息  
pcurr add_curr(pcurr curr_head,pcurr new_curr);             //课程信息加入链表  
void out_curr(pcurr curr_head);                             //输出课程信息  
pcurr del_curr(pcurr curr_head,pcurr del_point);            //删除课程信息  
pstu creat_stu(pstu stu_head);                              //创建结点，学生信息  
pstu find_stu(pstu stu_head,int number);                    //查找链表，学生信息  
pstu add_stu(pstu stu_head,pstu new_stu);                   //学生信息，加入链表 
void out_stu(pstu stu_head);                                //输出学生信息  
pstu del_stu(pstu stu_head,pstu del_point);                     //删除学生信息 
pstu choice_curr(pstu stu_head,int number1,pcurr curr_head);    //学生选课信息  
void out_choice(pstu stu_head);                                 //输出学生选课信息  

void title() 
{ 
    printf("\t------------欢迎使用西安工程大学--学生选课系统------------\n"); 
    printf("\t---------                                      ------------\n"); 
    printf("\t---------            1.输入课程信息            ------------\n"); 
    printf("\t---------            2.浏览课程信息            ------------\n"); 
    printf("\t---------            3.删除课程信息            ------------\n"); 
    printf("\t---------            4.输入学生信息            ------------\n"); 
    printf("\t---------            5.浏览学生信息            ------------\n"); 
    printf("\t---------            6.删除学生信息            ------------\n"); 
    printf("\t---------            7.学生选课              ------------\n"); 
    printf("\t---------            8.所有学生选课信息         ------------\n"); 
    printf("\t---------            9.退出程序              ------------\n"); 
    printf("\t---------                                      ------------\n"); 
}

int main() 
{ 
    int a,n=0,i; 
    int number=0; 
    int number1=0; 
    pcurr curr_head=NULL; 
    pstu stu_head=NULL; 
    pcurr new_curr=NULL; 
    pstu new_stu=NULL; 
    pcurr del_point=NULL; 
    char choice[20]; 
    do 
    { 
        system("cls"); 
        title(); 
        printf("请在1-9中选择:"); 
            scanf("%d",&a); 
        switch(a)
        { 
            case 1:  
            system("cls"); 
            new_curr=creat_curr(curr_head); 
            curr_head=add_curr(curr_head,new_curr); 
            break; 

            case 2: 
            system("cls"); 
            printf("\t\t---------            全部课程信息           ------------\n"); 
            printf("\t课程名称\t课程序号\t课程类型\t课程课时\t课程学分\n"); 
            out_curr(curr_head); 
            system("pause");   
            break; 

            case 3: 
            system("cls"); 
            printf("\t课程名称\t课程序号\t课程类型\t课程课时\t学分\n"); 
            out_curr(curr_head);  
            printf("请输入打算删除的课程的序号:\n"); 
                scanf("%d",&number); 
            curr_head=del_curr(curr_head,find_curr(curr_head,number)); 
            printf("删除成功！\n"); 
            system("pause"); 
            break; 

            case 4: 
            system("cls"); 
            new_stu=creat_stu(stu_head); 
            stu_head=add_stu(stu_head,new_stu); 
            break; 

            case 5: 
            system("cls"); 
            printf("\t\t---------            全部学生信息           ------------\n"); 
            printf("\t学生姓名\t学生学号\n"); 
            out_stu(stu_head); 
            system("pause");   
            break;

            case 6: 
            system("cls"); 
            printf("请输入打算删除的学生的学号:\n"); 
                scanf("%d",&number); 
            stu_head=del_stu(stu_head,find_stu(stu_head,number)); 
            printf("删除成功！\n"); 
            system("pause"); 
            break;
             
            case 7: 
            system("cls"); 
            printf("\t学生姓名\t学生学号\t学生性别\n"); 
            out_stu(stu_head); 
            printf("请输入选课学生学号:"); 
                scanf("%d",&number1); 
            choice_curr(stu_head,number1,curr_head); 
            system("pause"); 
            break; 
             
            case 8: 
            system("cls"); 
            printf("\t\t\t\t---------            全部选课信息           ------------\n"); 
            printf("\n"); 
            printf("\t学生姓名\t学生学号\t\n课程名称\t课程序号\t课程类型\t课程课时\t课程教室\n"); 
            out_choice(stu_head); 
            system("pause");   
            break;

            case 9: 
            return 0; 
            break; 
        } 
    }while(a!=0); 
    return 0;
} 
  
//创建结点，课程信息  
pcurr creat_curr(pcurr curr_head) 
{ 
    pcurr new_curr=(pcurr)malloc(sizeof(curr)); 
    printf("\n"); 
    printf("\t\t---------            输入课程信息           ------------\n"); 
    printf("\n"); 
    printf("请输入课程名称："); 
        scanf("%s",new_curr->name); 
    printf("请输入课程序号："); 
        scanf("%d",&new_curr->number); 
    printf("请输入课程类型："); 
        scanf("%s",new_curr->kind); 
    printf("请输入课程课时："); 
        scanf("%d",&new_curr->time); 
    printf("请输入该课程学分："); 
        scanf("%d",&new_curr->score); 
    while(find_curr(curr_head,new_curr->number)!=NULL) 
    { 
        printf("此序号已经有数据，请重新输入."); 
        scanf("%d",&new_curr->number); 
    } 
    new_curr->next=NULL; 
    return new_curr; 
} 

//查找课程信息的链表  
pcurr find_curr(pcurr curr_head,int number) 
{ 
    if(curr_head==NULL)     return NULL; 
    if(curr_head->number==number)   return curr_head; 
    return find_curr(curr_head->next, number);  
} 

//课程信息加入链表 
pcurr add_curr(pcurr curr_head,pcurr new_curr) 
{ 
    if(curr_head==NULL)     return new_curr; 
    new_curr->next=curr_head; 
    return new_curr; 
} 

 //输出课程信息  
void out_curr(pcurr curr_head) 
{ 
    while(curr_head){
        printf("\t%s\t\t%d\t\t%s\t\t%d\t\t%d\n",curr_head->name,curr_head->number,curr_head->kind,curr_head->time,curr_head->score); 
        curr_head=curr_head->next;
    }           
} 

//删除课程信息  
pcurr del_curr(pcurr curr_head,pcurr del_point) 
{ 
    pcurr point; 
    if(del_point==NULL){
        printf("没有此课程信息，请重新输入!\n"); 
        return curr_head;
    }  
    point=NULL; 
    if(del_point==curr_head){
        point=curr_head->next; 
        free(curr_head); 
        return point;
    }  
    point=curr_head; 
    while(point)
    { 
        if(point->next==del_point){ 
            point->next=del_point->next; 
            free(del_point); 
            return curr_head; 
        } 
        point=point->next; 
    }   
}  

//创建结点，学生信息  
pstu creat_stu(pstu stu_head) 
{ 
    pstu new_stu=(pstu)malloc(sizeof(stu)); 
    printf("\n"); 
    printf("\t\t---------            输入学生信息           ------------\n"); 
    printf("\n"); 
    printf("请输入学生姓名："); 
        scanf("%s",new_stu->name); 
    printf("请输入学生学号："); 
        scanf("%d",&new_stu->number); 
    while(find_stu(stu_head,new_stu->number)!=NULL) 
    { 
        printf("此学号已经有数据，请重新输入."); 
        scanf("%d",&new_stu->number); 
    } 
    new_stu->choices=NULL; 
    new_stu->next=NULL; 
    return new_stu; 
} 

//查找学生信息的链表  
pstu find_stu(pstu stu_head,int number) 
{ 
    if(stu_head==NULL)  return NULL; 
    if(stu_head->number==number)    return stu_head; 
    return find_stu(stu_head->next,number);  
} 

//学生信息加入链表  
pstu add_stu(pstu stu_head,pstu new_stu) 
{ 
    if(stu_head==NULL)  return new_stu; 
    new_stu->next=stu_head; 
    return new_stu; 
} 

//输出学生信息  
void out_stu(pstu stu_head) 
{ 
    while(stu_head){ 
    printf("\t%s\t\t%d\t\t%s\n",stu_head->name,stu_head->number); 
    stu_head=stu_head->next; 
    } 
} 

//删除学生信息  
pstu del_stu(pstu stu_head,pstu del_point) 
{ 
    pstu point; 
    if(del_point == NULL){ 
        printf("没有此学号信息，请重新输入!\n"); 
        return stu_head; 
    } 
    point=NULL; 
    if(del_point==stu_head){ 
        point=stu_head->next; 
        free(stu_head); 
        return point;
    } 
    point=stu_head; 
    while(point) 
    {
        if(point->next==del_point){ 
            point->next=del_point->next; 
            free(del_point); 
            return stu_head; 
        } 
        point=point->next; 
    } 
} 

//学生选课  
pstu choice_curr(pstu stu_head,int number1,pcurr curr_head) 
{ 
    int number; 
    pcurr point=NULL,point1=NULL; 
    pcurr choice_point=NULL; 
    pstu stu_point=find_stu(stu_head,number1); 
    if(stu_point!=NULL) 
    { 
        printf("\t课程姓名\t课程序号\t课程教师姓名\t课程课时\t学分\n"); 
        out_curr(curr_head); 
        printf("请输入所选课程序号："); 
            scanf("%d",&number); 
        point=find_curr(curr_head,number); 
        if(point!=NULL){ 
            choice_point=(pcurr)malloc(sizeof(curr)); 
            memcpy(choice_point,point,sizeof(curr)); 
            choice_point->next=NULL; 
            if(stu_point->choices==NULL){ 
                stu_point->choices=choice_point;
            } 
            else{ 
                choice_point->next=stu_point->choices; 
                stu_point->choices=choice_point; 
            } 
            printf("恭喜你！选课成功！\n"); 
            return stu_head; 
        } 
        else{ 
            printf("没有所选课程序号！"); 
            return stu_head; 
        } 
    } 
    else 
    { 
    printf("没有所选学生学号！"); 
    return stu_head; 
    } 
} 

//输出学生选课信息  
void out_choice(pstu stu_head) 
{ 
    pcurr point=NULL; 
    while(stu_head) 
    { 
        point=stu_head->choices; 
        printf("\t%s\t\t%d\t\t%s\n",stu_head->name,stu_head->number); 
        while(point) 
        { 
            printf("\t\t\t\t\t\t\t%s\t\t%d\t\t%s\t\t%d\t\t%d\n",point->name,point->number,point->kind,point->time,point->score); 
            point=point->next; 
        } 
        stu_head=stu_head->next; 
    } 
 } 
