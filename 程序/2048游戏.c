#include<stdio.h>
#include<ctype.h>
#include<stdlib.h>
#include<conio.h>
#include<time.h>
int g_gap=0;

void up(int str[][4])           //向上移动
{   
	int i,j,k,m=0,n=0;   //合并
	for(i=0;i<4;i++)
	{
		for(j=0;j<4;j++)
		{
			if(str[i][j]!=0)
			{
				m=i;
				k=i-1;
				for(;k>=0;k--)
				{
					if(str[m][j]==str[k][j])
					{
						str[k][j]=str[m][j]+str[k][j];
						str[m][j]=0;
					}
						if(str[k][j]!=0&&str[m][j]!=str[k][j])
							break;
						
				}
			}
		}
				
	}
	for(i=0;i<4;i++)//上移
	{	
		for(j=0;j<4;j++)
		{
			if(str[i][j]!=0)
			{   
				n=i;
				k=i-1;
				for(;k>=0;k--)
				{
					if(str[k][j]==0)
					{
						str[k][j]=str[n][j];
						str[n][j]=0;
						n--;
					}
				}
			}
		}
	}
}
void down(int str[][4])    //向下移动
{
	int i,j,k,m=0,n=0;
	for(i=3;i>=0;i--)   //合并
	{
		for(j=3;j>=0;j--)
		{
			if(str[i][j]!=0)
			{
				m=i;
				k=i+1;
				for(;k<4;k++)   
				{
					if(str[m][j]==str[k][j])
					{
						str[k][j]=str[m][j]+str[k][j];
						str[m][j]=0;	
					}
					if(str[k][j]!=0&&str[m][j]!=str[k][j])
					break;
				}
			}
		}
				
	}
	for(i=3;i>=0;i--)   //下移
	{	
		for(j=3;j>=0;j--)
		{
			if(str[i][j]!=0)
			{   
				n=i;
				k=i+1;
				for(;k<4;k++)
				{
					if(str[k][j]==0)
					{
						str[k][j]=str[n][j];
						str[n][j]=0;
						n++;
					}
				}
			}
		}
	}
}
void left(int str[][4])  //向左移动
{
	int i,j,k,m=0,n=0;
	for(i=0;i<4;i++)   //合并
	{
		for(j=0;j<4;j++)
		{
			if(str[i][j]!=0)
			{
				m=j;
				k=j-1;
				for(;k>=0;k--)  
				{
					if(str[i][m]==str[i][k])
					{
						str[i][k]=str[i][m]+str[i][k];
						str[i][m]=0;	
								
					}
				if(str[i][k]!=0&&str[i][m]!=str[i][k])
				break;
				}
			}
		}
				
	}
	for(i=0;i<4;i++)   // 左移
	{	
		for(j=0;j<4;j++)
		{
			if(str[i][j]!=0)
			{   
				n=j;
				k=j-1;
				for(;k>=0;k--)
				{
					if(str[i][k]==0)
					{
						str[i][k]=str[i][n];
						str[i][n]=0;
						n--;
					}
					
				}
			}
		}
	}	
}
void right(int str[][4])    //向右移动
{
	int i,j,k,m=0,n=0;
	for(i=3;i>=0;i--)   //合并
	{
		for(j=3;j>=0;j--)
		{
			if(str[i][j]!=0)
			{
				m=j;
				k=j+1;
				for(;k<4;k++)   
				{
					if(str[i][m]==str[i][k])
						{
							str[i][k]=str[i][m]+str[i][k];
							str[i][m]=0;	
								
						}
						if(str[i][k]!=0&&str[i][m]!=str[i][k])
							break;
				}
			}
		}
				
	}
	for(i=3;i>=0;i--)   //右移
	{	
		for(j=3;j>=0;j--)
		{
			if(str[i][j]!=0)
			{   
				n=j;
				k=j+1;
				for(;k<4;k++)
				{
					if(str[i][k]==0)
					{
						str[i][k]=str[i][n];
						str[i][n]=0;
						n++;
					}
				}
			}
		}
	}
}

int Over(int str[][4])//判断循环是否结束
{
	int t=0,i,j;
	for(i=0;i<4;i++)
	{
		for(j=0;j<4;j++)
		{
			if(str[i][j]==0)//判断是否有位置还未满，若有返回1游戏继续
			{
				return 1;
			}
			
		}
	}
	
	for(i=0;i<3;i++)  //注意防止越界
	{
		for(j=0;j<3;j++)
		{
			if(str[i][j]==str[i+1][j]||str[i][j]==str[i][j+1])//判断周围是否还有和自己相同的值，若有游戏继续
			{
				return 1;
			}
		}
	}
	for(i=0;i<4;i++)
	{
		for(j=0;j<4;j++)
		{
			if(str[i][j]==2048)//如果出现2048 返回0游戏结束
			{
				return 0;
			}
		}
	}
			
		return 0;//若无出现返回-1，则返回0游戏结束
}

int Rsl(int str[][4])  //游戏结果
{
	int i,j,rsl = 0;
	for(i=0;i<4;i++)
	{
		for(j=0;j<4;j++)
		{
			if(str[i][j]==2048)
			{
				rsl = 1;
			}
		}
	}
	if(rsl)
		return 1;
	else
		return 0;
	
}

void show(int str[][4])
{
	system("cls");
	for(int i=0;i<4;i++)
	{
		for(int j=0;j<4;j++)
		{
			printf("%3d  ",str[i][j]);
		}
		printf("\n");
	}
}

void rnum(int str[][4])//获得随机数
{
	int x,y,num,i,j,t=0;
	for(i=0;i<4;i++)
	{
		for(j=0;j<4;j++)
		{
			if(str[i][j]==0)
				t=1;          //判断二维数组还有没有空位
		}
	}
	if(t==0)	              //没有空位便返回
		return;
	srand(time(NULL)+g_gap++);
	x=rand()%4;
	y=rand()%4;
	num=(rand()%2+1)*2;
	if(str[x][y]==0)
	{
		str[x][y]=num;    //给随机一个有空位的赋值2或4
	}
	else if(str[x][y]!=0)
	{
		for(i=0;i<4;i++)
			for(j=0;j<4;j++)
				if(str[i][j]==0)
					{
						str[i][j]=num;
						i=4;//在这里使用i=4 j=4 与两层循环使用break等价
						j=4;
					}
	}
}

int main()
{  
	int i,j,num1,num2,m=0,n=0,str[4][4]={0};
	int x1,y1,x2,y2;
	srand((int)time(0));
	x1=rand()%4;
	y1=rand()%4;
	x2=rand()%4;
	y2=rand()%4;
	while(x1==x2&&y1==y2)
	{
		x1=rand()%4;
		y1=rand()%4;
		x2=rand()%4;
		y2=rand()%4;
	}
	num1=(rand()%2+1)*2;
	num2=(rand()%2+1)*2;
	str[x1][y1]=num1;
	str[x2][y2]=num2;
	for(i=0;i<4;i++)
	{
		for(j=0;j<4;j++)
			printf("%3d  ",str[i][j]);
		printf("\n");
	}

	char ch=getch();
	 while(Over(str))
	 {
		 if(ch==72)
		 {
			 up(str);
			 rnum(str);
			 show(str);
		 }
		 if(ch==80)
		 {			
			 down(str);
			 rnum(str);
			 show(str);
		 }
		 if(ch==75)
		 {
			 left(str);
			 rnum(str);
			 show(str);
		 }
		 if(ch==77)
		 {
			 right(str);
			 rnum(str);
			 show(str); 
		 }
		  ch=getch();
	}
	 if(Rsl(str))
		 printf("游戏胜利\n");
	 else
		 printf("游戏失败\n");

}

