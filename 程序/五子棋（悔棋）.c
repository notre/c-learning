#include<stdio.h>
#include<stdlib.h>
#define MAXSIZE 400
#define true 1
#define false 0
int a[20][20]={0},b[20][20]={0},sum=0;    //a下棋用，b悔棋用，sum记录落子数，队列用来复盘

typedef struct 
{
    int hang;
    int lei;
}Element;
typedef struct LinkQueueNode
{
    Element data;
    struct LinkQueueNode *next;
}LinkQueueNode;
typedef struct
{
    LinkQueueNode *front;
    LinkQueueNode *rear;
}LinkQueue;

int Init(LinkQueue *Q)
{
    Q->front=(LinkQueueNode *)malloc(sizeof(LinkQueueNode));
    if(Q->front!=NULL){
        Q->rear=Q->front;
        Q->front->next=NULL;
        return(true);
    }
    return(false);
}

int Enter(LinkQueue *Q,int p,int q)
{
    LinkQueueNode *NewNode;
    NewNode=(LinkQueueNode*)malloc(sizeof(LinkQueueNode));
    if(NewNode!=NULL){
        NewNode->data.hang=p-1;
        NewNode->data.lei=q-1;
        NewNode->next=NULL;
        Q->rear->next=NewNode;
        Q->rear=NewNode;
        return(true);
    }
    return(false);
}

int Delete(LinkQueue *Q,int *x,int *y)
{
    LinkQueueNode *p;
    if(Q->front==Q->rear)
        return(false);
    p=Q->front->next;
    Q->front->next=p->next;
    if(Q->rear==p)
        Q->rear=Q->front;
    *x=p->data.hang;
    *y=p->data.lei;
    free(p);
    return(true);
}

void qipan(int i,int j)     //打印当前棋盘
{
    for(int m=0;m<i;m++){
        for(int n=0;n<j;n++){
            if(a[m][n]==0){
                printf(". ");
            }
            else if(a[m][n]==1){
                printf("@ ");
            }
            else{
                printf("# ");
            }
        }
        printf("\n");
    }
}

int fun(int row,int col,int x)      //判断是否构成五子
{
    int m=col-1,n=col+1;
    int notre=1;
    //横向判断
    for(int i=col-1;a[row][i]==x;i--)
        notre++;
    for(int i=col+1;a[row][i]==x;i++)
        notre++;
    if(notre==5){
        return x;
    }    
    else notre=1;
    //竖向判断
    for(int i=row-1;a[i][col]==x;i--)
        notre++;
    for(int i=row+1;a[i][col]==x;i++)
        notre++;
    if(notre==5){
        return x;
    }
    else notre=-1;
    //斜向判断(左斜)
    for(int i=row,j=col;a[i][j]==x;i--,j--){
        notre++;
    }
    for(int i=row,j=col;a[i][j]==x;i++,j++){
        notre++;
    }
    if(notre==5){
        return x;
    }
    else notre=-1;
    //斜向判断(右斜)
    for(int i=row,j=col;a[i][j]==x;i--,j++){
        notre++;
    }
    for(int i=row,j=col;a[i][j]==x;i++,j--){
        notre++;
    }
    if(notre==5){
        return x;
    }
}

void fupan(LinkQueue *Q)
{
    int i,x,y;
    printf("\nfupanma?");
    printf("\n1 Yes,2 No:");
    scanf("%d",&i);
    if(i==1){
        while(Q->front!=Q->rear){
            Delete(Q,&x,&y);
            printf("%d,%d\n",x+1,y+1);
        } 
        exit(0);
    }
    else exit(0);
}

int main()
{
    LinkQueue Q;
    Init(&Q);
    int p=0,q=0;
    int m,n;
    qipan(20,20);
    printf("shuru 0,0 hiuqi(shang yibu)\n");
    while(1){
        printf("baiqi zuobiao:");
        scanf("%d,%d",&p,&q);
        if(p==0&&q==0){
            Enter(&Q,p,q);
            a[m-1][n-1]=0;
            sum--;
        }  
        else if(a[p-1][q-1]==0&&p<=20&&p>=1&&q<=20&&q>=1){
            a[p-1][q-1]=1;
            Enter(&Q,p,q);
            m=p,n=q;
            sum++;}
        else continue;
        system("cls");
        qipan(20,20);
        if(fun(p-1,q-1,1)==1){
            printf("baiqi ying!");
            fupan(&Q);
            exit(0);}  

        while(1){
            printf("heiqi zuobiao:");
            scanf("%d,%d",&p,&q);
            if(p==0&&q==0){
                a[m-1][n-1]=0;
                Enter(&Q,p,q);
                sum--;
                break;}
            if(a[p-1][q-1]==0&&p<=20&&p>=1&&q<=20&&q>=1){
                a[p-1][q-1]=2;
                Enter(&Q,p,q);
                m=p,n=q;
                sum++;
                break;}
        }
        system("cls");
        qipan(20,20);
        if(fun(p-1,q-1,2)==2){
            printf("heiqi ying!");
            fupan(&Q);
            exit(0);}

        if(sum==400){
            printf("pingju!");
            exit(0);}   
    }
    return 0;
}